.. index::
   ! meta-infos


.. _doc_meta_infos:

=====================
Meta infos
=====================

- https://cnt-f.gitlab.io/meta/


Gitlab project
================

- https://gitlab.com/cnt-f/statuts

Issues
--------

- https://gitlab.com/cnt-f/statuts/-/boards

Fichier sources
----------------

- https://gitlab.com/cnt-f/statuts/-/tree/main


Pipelines
-----------

.. seealso::

   - https://gitlab.com/cnt-f/statuts/-/pipelines


Membres
-----------

- https://gitlab.com/cnt-f/statuts/-/project_members


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
