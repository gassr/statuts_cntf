
.. index::
   pair: Chartes ; Paris
   ! Charte de Paris

.. _charte_de_paris:
.. _chartes_de_paris:

==================================================================
Charte de Paris dite **du syndicalisme révolutionnaire** (1946)
==================================================================

.. seealso::

   - http://www.cnt-f.org/la-charte-de-paris-1946.html




:download:`Télécharger Chartes de Paris dite "du syndicalisme révolutionnaire" 1946 <La_Charte_du_Syndicalisme_Revolutionnaire_dite_de_Paris.doc>`


La Charte du Syndicalisme Révolutionnaire adoptée au Congrès constitutif de la C.N.T - Décembre 1946.
=======================================================================================================

En présence de l'instabilité politique et financière de l'Etat français, qui
peut à tout instant provoquer une crise de régime et, par conséquent, poser la
question d'un ordre social nouveau par les voies révolutionnaires, le Congrès,
en même temps qu'il se refuse à donner au capitalisme le moyen de se rééquilibrer,
déclare que le syndicalisme doit tirer de cette situation catastrophique le
maximum de résultats pour l'affranchissement des travailleurs.

En conséquence, il affirme que les efforts du prolétariat doivent tendre, non
seulement à renverser le régime actuel, mais encore à rendre impossible la
prise du pouvoir et son exercice par tous les partis politiques qui s'en
disputent déjà âprement la possession.

C'est ainsi que le syndicalisme doit savoir profiter de toutes les tentatives
faites par les partis pour s'emparer du pouvoir, pour jouer lui-même son rôle
décisif qui consiste à détruire ce pouvoir et à lui substituer un ordre social
reposant sur l'organisation de la production, de l'échange et de la répartition
dont le fonctionnement sera assuré par le jeu des rouages syndicaux à tous
les degrés.

En proclamant le sens profondément économique de la révolution prochaine,
le Congrès tient à préciser essentiellement qu'elle doit revêtir un caractère
de radicale transformation sociale devenue indispensable et reconnue inévitable
aussi bien par le capitalisme que par le prolétariat.

Ce caractère ne peut lui être imprimé sur le plan de classe des travailleurs
que par le prolétariat organisé dans les syndicats, en dehors de toute autre
direction extérieure, qui ne peut que lui être néfaste.

C'est seulement à cette condition que les soubresauts révolutionnaires des
peuples, jusqu'ici utilisés et dirigés par les partis politiques, permettront
enfin d'apporter un changement notable dans l'ordre économique et social,
ainsi que l'exige le développement des sociétés modernes.

En considération de ce qui précède, le Congrès déclare que les évènements
prochains, en se déroulant dans l'ordre économique, vont poser les nouvelles
conditions de vie des peuples et fixer avec une force grandissante
et insoupçonnée les véritables caractères de la vie sociale.

Cette vie sera l'oeuvre des forces productrices et créatrices, associant
harmonieusement les efforts des manoeuvres, des techniciens et des savants,
orientés constamment vers le progrès.

Ainsi se précisent logiquement les caractères de la transformation nécessaire.

Reprenant les termes de cette partie de la résolution d'Amiens qui déclare que
le syndicat, aujourd'hui groupement de résistance, sera, dans l'avenir,
le groupement de production et de répartition, base de la réorganisation
sociale, Le Congrès affirme que le syndicalisme, expression naturelle et
concrète du mouvement des producteurs, contient à l'état latent et organique
toutes les activités d'exécution et de direction capables d'assurer la vie
nouvelle. Il lui appartient donc, dès maintenant, de rassembler sur un plan
uniquement d'organisation toutes les forces de la main-d'oeuvre, de la
technique et de la science, agissant séparément, en ordre dispersé, dans
l'industrie et aux champs.

En réunissant, dès que possible, dans un même organisme toutes les forces qui
concourent à assurer la vie sociale, le syndicalisme sera en mesure, dès le
commencement de la révolution, de prendre en main, par tous ses organes, la
direction de la production et l'administration de la vie sociale.

Comprenant toute la grandeur et toute la difficulté de ce devoir, le Congrès
tient à affirmer que le syndicalisme doit, dès maintenant, remanier son
organisation, compléter ses organes, les adapter aux nécessités,
comme le capitalisme lui-même, et se préparer à agir, demain, en
administrateur et en gestionnaire éclairé de la production, de la répartition
et de l'échange.

Il ne méconnaît pas l'extrême complexité des problèmes qui seront posés par la
disparition du capitalisme. Aussi, il n'hésite pas à déclarer que le mouvement
des travailleurs, qui ne recèle pas encore toutes les forces nécessaires à la
vie sociale de demain, doit faire la preuve de son intelligence et de sa
souplesse en appelant à lui tous les individus, toutes les activités qui, par
leurs fonctions, leur savoir, leurs connaissances, ont leur place naturelle
dans son sein et seront indispensables pour assurer la vie nouvelle à tous
les échelons de la production.

N'ignorant pas les changements profonds qui sont survenus dans le domaine de
la science et de la technique, que ce soit dans l'industrie ou dans
l'agriculture, le Congrès, préoccupé des transformations nécessaires,
n'hésite pas à faire appel aux savants et aux techniciens.

De même, il s'adresse aux paysans, pour assurer conjointement avec leurs frères
ouvriers la vie et la défense de la révolution qui ne saurait s'effectuer sans
leur concours éclairé, constant et complet. Le Congrès pense qu'ainsi se
scellera, par un effort concordant, harmonieux et fécond, qui les rassemblera
tous pour une même tâche de libération humaine, l'union des travailleurs de
la pensée et des bras, de l'industrie et des champs.

N'ayant pour unique ambition que d'être les pionniers hardis d'une
transformation sociale dont les agents d'exécution et de direction oeuvreront
sur le plan du syndicalisme, les syndicalistes désirent que leur mouvement,
vivant reflet des aspirations et des besoins matériels et moraux de l'individu,
devienne la véritable synthèse d'un mécanisme social déjà en voie de
constitution où tous trouveront les conditions organiques, idéalistes et
humaines de la révolution prochaine, désirée par tous les travailleurs.

Demain doit être aux producteurs, groupés ou associés, en vertu de leurs
fonctions économiques. L'organisation politique et sociale surgira de
leur sein. Elle portera en elle-même tous les facteurs de réalisation,
organisation, cohésion, impulsion et action.

De cette façon se dressera en face du citoyen : entité fuyante, instable et
artificielle, le travailleur : réalité vivante, support logique et moteur
naturel des sociétés humaines.

Le syndicalisme dans le cadre national
======================================

Son action générale
--------------------

La Confédération Nationale du Travail affirme, dès sa constitution, qu'elle
entend être exclusivement un groupement de classe : celui des travailleurs.

Elle doit donc, en plein accord sur ce point avec la Charte d'Amiens, mener la
lutte sur le terrain économique et social.

Véritable organisme de défense et de lutte de classes, elle est, en dehors de
tous les partis et en opposition avec ceux-ci, la force active qui doit
permettre à tous les travailleurs de défendre leurs intérêts immédiats et
futurs, matériels et moraux. S'inspirant de la situation présente, elle
déclare vouloir préparer sans délai les cadres complets de la vie sociale et
économique de demain, dont elle tient à examiner tout de suite les caractères
possibles et le fonctionnement général.

Au capitalisme - conséquence et résultante de la vie passée, adaptée et façonnée
par les forces dirigeantes en dehors de toute doctrine comme de toute théorie -
entrant dans le dernier cycle de son évolution historique, le Congrès entend
substituer le syndicalisme, expression naturelle de la vie sociale des individus
en marche vers le communisme libre.

Rejetant le principe du partage des privilèges chers aux défenseurs de
l'intérêt général et de la superposition des classes qui est aussi celui de
nos adversaires, le syndicalisme doit poursuivre sa mission qui est:

- de détruire les privilèges,
- d'établir l'égalité sociale.

Il n'atteindra ce but qu'en faisant disparaître le patronat, en abolissant le
salariat individuel ou collectif et en supprimant l'Etat.

Il préconise à ce sujet la grève générale, l'expropriation capitaliste et la
prise de possession des moyens de production et d'échange, ainsi que la
destruction immédiate de tout pouvoir étatique.

Ses moyens d'action
-------------------

Précisant sa conception de la grève générale, le Congrès tient à déclarer
très fermement que ce moyen d'action conserve à ses yeux toute sa valeur, en
toutes circonstances, que ce soit corporativement, régionalement, nationalement
ou internationalement.

Que ce soit pour faire triompher les revendications particulières ou générales,
fédérales ou nationales, offensivement ou défensivement, pour protester contre
l'arbitraire patronal ou gouvernemental, la grève, partielle ou générale,
reste et demeure la seule arme du prolétariat.

En ce qui concerne la grève générale expropriatrice, premier acte
révolutionnaire qui sera marqué par la cessation immédiate et simultanée du
travail en régime capitaliste, le Congrès affirme qu'elle ne peut être que
violente. Elle aura pour objectif :

1. de priver le capitalisme et l'Etat de toute possibilité d'action en
   s'emparant des moyens de production et d'échange et de chasser du pouvoir
   ses occupants du moment ;
2. de défendre les conquêtes prolétariennes qui doivent permettre d'assurer
   l'existence de l'ordre nouveau ;
3. de remettre en marche l'appareil de la production et des échanges, après
   avoir réduit au minimum - pour la prise de possession - le temps d'arrêt
   de la production et des échanges ruraux et urbains ;
4. de remplacer le pouvoir étatique détruit par une organisation fédéraliste et
   rationnelle de la production, de l'échange et de la répartition.

Confiant dans la valeur de ce moyen de lutte, le Congrès déclare que le
prolétariat, non seulement saura prendre possession de toutes les forces de
production, détruire le pouvoir étatique existant, mais encore sera capable
d'exploiter ces forces dans l'intérêt de la collectivité affranchie et de les
défendre contre toute entreprise contre-révolutionnaire, les armes à la main,
et de donner à l'organisation sociale la forme qu'exigera le stade d'évolution
atteint par les individus vivant à cette époque.

Il déclare que le terme des conquêtes révolutionnaires ne peut être marqué que
par les facultés de compréhension des travailleurs et les possibilités de
réalisation de leurs organismes économiques, dont l'effort devra être porté
au maximum.

Par là, le Congrès indique que la stabilisation momentanée de la révolution
doit s'accomplir en dehors de tout système préconçu, de tout dogme, comme de
toute théorie abstraite, qui seraient pratiquement en contradiction avec les
faits de la vie économique qui doit nécessairement donner naissance à la vie
politique et sociale exprimant l'ordre nouveau.

Proclamant son attachement indéfectible à la lutte révolutionnaire, le Congrès
tient, pour bien préciser sa pensée, à déclarer qu'il considère la révolution
comme un fait social, déterminé par la contradiction permanente des intérêts
des classes en lutte, qui vient tout à coup marquer brutalement leur
antagonisme en rompant le cours normal de leur évolution qu'il tend
à précipiter.

En conséquence, il déclare que le syndicalisme (comme tous les autres mouvements)
a le droit de l'utiliser, suivant ses desseins, pour atteindre le maximum des
buts qu'il s'est fixé, sans confondre son action avec celles des partis qui
prétendent, eux aussi, transformer l'ordre politique et social et préconisent
pour cela la dictature prolétarienne et la constitution d'un Etat
soi-disant provisoire.

En dehors de cette action essentielle, le Congrès déclare que, par son action
revendicatrice quotidienne, le syndicalisme poursuit la coordination des
efforts ouvriers, l'accroissement du mieux-être des travailleurs par la
réalisation d'améliorations immédiates, telles que : la diminution
des heures de travail, l'augmentation des salaires, etc., il prépare chaque
jour l'émancipation des travailleurs qui ne sera réalisée que par
l'expropriation du capitalisme.

En condamnant la "collaboration des classes" et "le syndicalisme d'intérêt général ",
le Congrès tient à déclarer que ce ne sont pas les discussions inévitables
entre patrons et ouvriers qui constituent des actes de collaboration
de classes.

En ne voyant dans ces discussions qui résultent de l'état de choses actuel
qu'un aspect de la lutte permanente des classes, le Congrès précise que la
collaboration des classes est caractérisée par le fait de participer, dans
des organismes réunissant des représentants des ouvriers, des patrons ou
de l'Etat, à l'étude en commun des problèmes économiques dont la solution
apportée ne saurait que prolonger, en la renforçant, l'existence du
régime actuel.

Le syndicalisme dans la période pré-révolutionnaire
===================================================

Considérant que dans la période pré-révolutionnaire le rôle du syndicalisme est
de dresser une opposition constante aux forces capitalistes, de diminuer le
pouvoir patronal en augmentant celui du syndicat, le Congrès estime que ces
résultats ne peuvent être obtenus que par l'introduction du contrôle syndical
dans les entreprises capitalistes, par la création des comités et des conseils
d'ateliers, d'usines, de bureaux, de chantiers, de gares, de ports, de fermes
ou d'exploitations agricoles dans tous les domaines de la production.

En même temps que sera menée à bien la besogne de documentation, d'éducation
technique et professionnelle en vue de la réorganisation sociale, sera enfin
réalisé, dans les meilleures conditions, l'apprentissage de classe
de la gestion.

En indiquant que les syndicats constitueront les cadres de la société nouvelle,
le Congrès déclare qu'en ouvrant l'accès du syndicat aux techniciens et aux
savants, ceux-ci s'y trouveront placés sur un pied de complète égalité avec
les autres travailleurs.

C'est de la collaboration intelligente et amicale de tous ces éléments que
surgira le véritable Conseil économique du travail, qui aura pour mission
de poursuivre le travail de préparation à la gestion des moyens de production,
d'échange et de répartition et aura à charge, sous la direction des Congrès,
de chercher les moyens les meilleurs pour faire aboutir les
revendications ouvrières.

Rapport du syndicalisme avec les autres forces révolutionnaires
===============================================================

Le Congrès affirme à nouveau que le syndicalisme doit vivre et se développer
dans l'indépendance absolue, qu'il doit jouir de l'autonomie complète qui
convient à son caractère de force essentielle de la révolution.

Par sa doctrine, ses buts, son action corporative et sociale, le syndicalisme
s'affirme comme le seul mouvement de classe des travailleurs. Il est capable
de réaliser, par lui-même, aux différents stades de l'évolution humaine,
aussi bien le communisme organisé que le communisme libre.

Cela implique qu'il ne peut concourir à la poursuite des objectifs politiques
affirmés par les partis et qu'il ne peut lier son action à la leur.

L'affirmation sans cesse plus nette des buts poursuivis par les autres
confédérations syndicales et leurs partis oblige la C.N.T à répudier toutes
alliances avec ces forces sur le terrain révolutionnaire.

En effet, s'il est encore possible de réunir dans une action corporative
commune toutes les forces ouvrières groupées dans les différentes
confédérations syndicales, il est indéniable que toute conjugaison de ces
mêmes forces pour une lutte révolutionnaire apparaît inutile et vaine en
raison de l'opposition fondamentale des buts que se sont assignés les
diverses fractions du syndicalisme.

De toute évidence, cette incompatibilité d'action révolutionnaire s'étend
"a fortiori" aux ententes avec les partis politiques ouvriers qui, tous,
sans exception, veulent et c'est leur raison d'être - instaurer un Etat
politique dont ils auraient la direction.

Etat dont le syndicalisme révolutionnaire proclame la nocivité et nie la
nécessité.

En conséquence, le Congrès déclare que la C.N.T. ne peut unir ses efforts à
ceux des autres confédérations syndicales que sur le terrain de l'action
quotidienne. Il est d'ailleurs persuadé que l'unité de toutes les forces
révolutionnaires se réalisera sur le terrain de classe, dans la phase
décisive de destruction de l'Etat bourgeois et du capitalisme pour se
continuer dans la période constructive, qu'elle se scellera par l'entrée
de tous les travailleurs dans leur groupement naturel : le syndicat,
organe complet de production, d'administration et de défense d'une société
reposant exclusivement sur le travail, sa répartition, son échange, de la base
au faîte de son édifice.

Le syndicalisme dans le cadre international
===========================================

Considérant que, plus que jamais, les travailleurs ont pour devoir de se tendre
la main par-dessus les frontières et de proclamer qu'ils appartiennent à une
même classe - celle des exploités.

Le Congrès estime que, pour opposer un front unique, commun et irrésistible à
la puissance capitaliste, les ouvriers doivent se réunir au sein d'un organisme
international dans lequel ils retrouveront le prolongement de leur propre
action de classe qu'ils engagent dans chaque pays, contre leur patronat
respectif.

Il estime que la place d'un mouvement syndical basé sur la lutte de classes ne
peut être que dans une Internationale qui accepte les principes suivants :
autonomie complète, indépendance absolue du syndicalisme dans l'administration,
la propagande, la préparation de l'action, dans l'étude des moyens
d'organisation et de lutte future et dans l'action ellemême.

Ayant ainsi défini sa compréhension de l'action du syndicalisme révolutionnaire
sur le terrain national et international, le Congrès donne l'adhésion de la
C.N.T. à l'Association Internationale des Travailleurs.

Il proclame que cette Internationale est la continuation logique de la
Première Internationale, de même que la C.N.T est la continuation de la
C.G.T. de 1906.

Réorganisation des jeunesses syndicalistes
==========================================

Considérant que le développement et l'avenir du mouvement syndical résident en
grande partie dans la formation sans cesse renouvelée de ses cadres, Le Congrès
décide que l'éducation des jeunes ouvriers doit redevenir une des principales
préoccupations du syndicalisme.

En conséquence, il fait obligation très précise aux organismes syndicaux à tous
les degrés de reconstituer, sous la direction effective de la C.N.T, les
jeunesses syndicalistes.

Il spécifie que les jeunes n'ayant pas à déposer ou à défendre des
revendications, ce qui est du ressort du syndicat, devront recevoir une large
éducation sociale qui doit leur être donnée par les unions locales, avec le
concours des syndicats.

Les groupements locaux, régionaux et nationaux de jeunes participeront à titre
consultatif aux assemblées de même nature de la C.N.T. Ils devront, dès que
possible, aussitôt qu'ils auront acquis les notions indispensables, être
adjoints aux militants locaux, régionaux ou nationaux responsables de la
marche des différents rouages du syndicalisme pour s'initier à leur
fonctionnement.

En outre, le Congrès charge le Bureau confédéral de présenter au prochain
C.C.N. un plan complet d'organisation des jeunesses syndicalistes.
