
.. index::
   Chartes (de Lyon)


.. _chartes_de_lyon:

===============================
Chartes de Lyon (1926)
===============================

:download:`Télécharger Chartes de Lyon 1926 <La_Charte_de_Lyon.doc>`


adoptée par le congrès de la CGT-SR en 1926.

En présence de l'instabilité politique et financière de l'Etat français, qui
peut â tout instant provoquer une crise de régime et par conséquent, poser la
question d'un ordre social nouveau par les voies révolutionnaires, le congrès,
en même temps qu'il se refuse à donner au capitalisme les moyens de
rééquilibrer, déclare que le syndicalisme doit tirer de cette situation
catastrophique le maximum de résultats pour l'affranchissement des travailleurs.

En conséquence, il affirme que les efforts du prolétariat doivent tendre, non
seulement à renverser le régime actuel, mais encore à rendre impossible la
prise du pouvoir et son exercice par tous les partis politiques qui s'en
disputent âprement la possession.

C'est ainsi que le syndicalisme doit savoir profiter de toutes les tentatives
faites par les partis, pour s'emparer du pouvoir, pour jouer lui-même son rôle
décisif qui consiste à détruire ce pouvoir et à lui substituer un ordre social
reposant sur l'organisation de la production de l'échange et de la répartition,
dont le fonctionnement sera assuré par le jeu des rouages syndicaux à tous les
degrés.

En proclamant le sens profondément économique de la révolution prochaine, le
congrès tient à préciser essentiellement, qu'elle doit revêtir un caractère
de radicale transformation sociale devenue indispensable et reconnue
inévitable aussi bien par le capitalisme que par le prolétariat.

Ce caractère ne peut lui être imprimé sur le plan de classe des travailleurs
que par le prolétariat organisé dans les syndicats, en dehors de toute autre
direction extérieure, qui ne peut que lui être néfaste.

C'est seulement à cette condition que les soubresauts révolutionnaires des
peuples, jusqu'ici utilisés et dirigés par les partis politiques, permettront
enfin d'apporter un changement notable dans l'ordre économique et social, ainsi
que l'exige le développement des sociétés modernes.

Le congrès constate la profonde nouveauté des événements qui se préparent et
rendent inutiles et impossibles les transformations politiques partielles.

Il enregistre aussi que le fascisme, nouvelle doctrine de gouvernement des
puissances d'argent, qui commandent à tout le système capitaliste, pose
lui-même le problème social sous le même angle économique et entend utiliser
le syndicalisme en l'adaptant à ses vues particulières pour réaliser
ses desseins.

En considération de ce qui précède, le congrès déclare que les événements
prochains en se déroulant dans l'ordre économique vont poser les nouvelles
conditions de vie des peuples et fixer, avec une force grandissante et
insoupçonnée, les véritables caractères de la vie sociale.

Cette vie sera l'oeuvre des forces productives et créatrices associant
harmoniquement les efforts des manoeuvres, des techniques et des savants,
orientés constamment vers le progrès.

Ainsi se précisent logiquement les caractères de la transformation nécessaire.
Reprenant les termes de cette partie de la résolution d'Amiens, qui déclare
que "le Syndicat aujourd'hui groupement de résistance, sera, dans l'avenir,
le groupement de production et de répartition, base de la réorganisation sociale",
le congrès affirme que **le syndicalisme, expression naturelle et concrète du
mouvement des producteurs, contient à l'état latent et organique, toutes les
activités d'exécution et de direction capables d'assurer la vie nouvelle**.

Il lui appartient donc, dès maintenant, de rassembler sur un plan unique
d'organisation, toutes les forces de la main-d'oeuvre, de la technique et de
la science, agissant séparément, en ordre dispersé, dans l'industrie et
aux champs.

En réunissant, dès que possible, dans un même organisme toutes les forces qui
concourent à assurer la vie sociale, le syndicalisme sera en mesure, dès le
commencement de la révolution, de prendre en mains, par tous ses organes, la
direction de la production et l'administration de la vie sociale.

Comprenant toute la grandeur et toute la difficulté de ce devoir, le congrès
tient à affirmer que le syndicalisme doit, dès maintenant, remanier son
organisation, compléter ses organes, les adapter aux nécessités -comme le
capitalisme lui-même- et se préparer à agir, demain, en administrateur et en
gestionnaire éclairé de la production, de la répartition et de l'échange.

Il ne méconnaît pas l'extrême complexité des problèmes qui seront posés par la
disparition du capitalisme.

Aussi, il n'hésite pas à déclarer que le mouvement des travailleurs, qui ne
recèle pas encore toutes forces nécessaires à la vie sociale de demain, doit
faire la preuve de son intelligence et de sa souplesse en appelant à lui tous
les individus, toutes les activités qui, par leurs fonctions, leur savoir,
leurs connaissances, ont leur place naturelle dans son sein et seront
indispensables pour assurer la vie nouvelle à tous les échelons de la production.

N'ignorant pas les changements profonds qui sont survenus dans le domaine de
la science et de la technique, que ce soit dans l'industrie et dans
l'agriculture, le congrès, préoccupé des transformations nécessaires,
n'hésite pas à faire appel aux savants et aux techniciens.

De même, il s'adresse aux paysans, pour assurer conjointement avec leurs frères
ouvriers la vie et la défense de la révolution qui ne saurait s'effectuer sans
leur concours éclairé, constant et complet.

Le congrès pense qu'ainsi se scellera, par un effort concordant, harmonieux et
fécond, qui les rassemblera tous pour une même tâche de libération humaine,
l'Union des travailleurs de la Pensée et des Bras, de l'industrie et des champs.

N'ayant pour unique ambition que d'être les pionniers hardis d'une
transformation sociale dont les agents d'exécution et de direction oeuvreront
sur le plan du syndicalisme, les syndicalistes désirent que leur mouvement,
vivant reflet des aspirations et des besoins matériels et moraux de l'individu,
devienne la véritable synthèse d'un mécanisme social déjà en voie de
constitution, où tous trouveront les conditions organiques, idéalistes et
humaines de la révolution prochaine, désirée par tous les travailleurs.

Demain doit être aux producteurs, groupés ou associés, en vertu de leurs
fonctions économiques. L'organisation politique et sociale surgira de leur
sein. Elle portera en elle-même, tous les facteurs de réalisation,
organisation, coordination, cohésion, impulsion et action.

De cette façon, se dressera en face du citoyen, entité fuyante, instable et
artificielle, le travailleur, réalité vivante, support logique et moteur
naturel des sociétés humaines.

Le syndicalisme dans le cadre national
======================================

Son action générale
====================

La C.G.T. syndicaliste révolutionnaire affirme, dès sa constitution, qu'elle
entend être exclusivement un groupement de classe : celui des travailleurs.

Elle doit donc, en plein accord sur ce point, avec la Charte d'Amiens, mener
la lutte sur le terrain économique et social. Véritable organisme de défense
et de lutte de classe, elle est en dehors de tous les partis et en
opposition avec ceux-ci, la force active qui doit permettre à tous les
travailleurs de défendre leurs intérêts immédiats et futurs, matériels
et moraux.

S'inspirant de la situation présente, elle déclare vouloir préparer, sans délai,
les cadres complets de la vie sociale et économique de demain, dont elle tient
à examiner tout de suite les caractères possibles et le fonctionnement général.

Au capitalisme -conséquence et résultante de la vie passée, adapté et façonné
par les forces dirigeantes en dehors de toute doctrine comme de toute théorie-
entrant dans le dernier cycle de son évolution historique, le congrès entend
substituer le syndicalisme, expression naturelle de la vie sociale des
individus en marche vers le communisme libre.

Rejetant le principe du partage des privilèges chers aux défenseurs de
l'intérêt général et de la superposition des classes -qui est aussi celui de
nos adversaires-, le syndicalisme doit poursuivre sa mission qui est:

- de détruire les privilèges,
- d'établir l'égalité sociale.

Il n'atteindra ce but qu'en faisant disparaître le patronat, en abolissant
le salariat individuel ou collectif et en supprimant l'Etat.

Il préconise à ce sujet, la grève générale, l'expropriation capitaliste et la
prise en possession des moyens de production et d'échange, ainsi que la
destruction immédiate de tout pouvoir étatique.

Ses moyens d'action
===================

Précisant sa conception de la grève générale, le congrès tient à déclarer
fermement que ce moyen d'action conserve, à ses yeux, toute sa valeur, en
toutes circonstances, que ce soit corporativement, localement, régionalement,
nationalement ou internationalement.

Que ce soit pour faire triompher les revendications particulières ou générales,
fédérales ou nationales, offensivement ou défensivement, la grève, partielle
ou générale, reste et demeure la seule et véritable arme du prolétariat.

En ce qui concerne la grève générale expropriatrice, premier acte
révolutionnaire qui sera marqué par la cessation immédiate, concertée et
simultanée du travail en régime capitaliste, le congrès affirme qu'elle ne
peut être que violente.

Elle aura pour objectifs:

- de priver le capitalisme et l'Etat de toute possibilité d'action, en
  s'emparant des moyens de production et d'échange, et de chasser du pouvoir
  ses occupants du moment ;

- de défendre les conquêtes prolétariennes qui doivent permettre d'assurer
  l'existence de l'ordre nouveau ;

- de remettre en marche, l'appareil de la production et des échanges, après
  avoir réduit, au minimum -pour la prise de possession- le temps d'arrêt de
  la production et des échanges ruraux et urbains ;

- de remplacer le pouvoir étatique détruit par une organisation fédéraliste
  et rationnelle de la production, de l'échange et de la répartition.

Confiant dans la valeur de ce moyen de lutte, le congrès déclare que le
prolétariat saura, non seulement prendre possession de toutes les forces de
production, détruire le pouvoir étatique existant, mais encore sera capable
d'utiliser ces forces dans l'intérêt de la collectivité affranchie et de les
défendre contre toute entreprise contre-révolutionnaire, les armes à la main,
et de donner à l'organisation sociale la forme qu'exigera le stade d'évolution
atteint par les individus vivant à cette époque.

Il déclare que le terme des conquêtes révolutionnaires ne peut être marqué que
par les facultés de compréhension des travailleurs et les possibilités de
réalisations de leurs organismes économiques, dont l'effort devra être porté
au maximum.

Par là, le congrès indique que la stabilisation momentanée de la révolution
doit s'accomplir en dehors de tout système préconçu, de tout dogme, comme de
toute théorie abstraite, qui seraient pratiquement en contradiction avec les
faits de la vie économique et sociale exprimant l'ordre nouveau.

Proclamant son attachement indéfectible à la lutte révolutionnaire, le
congrès tient, pour bien préciser sa pensée, à déclarer qu'il considère la
révolution comme un fait social déterminé par la contradiction permanente des
intérêts des classes en lutte, qui vient tout à coup marquer brutalement leur
antagonisme, en rompant le cours normal de leur évolution qu'il tend
à précipiter.

En conséquence, il déclare que le syndicalisme -comme tous les autres mouvements-
a le droit de l'utiliser, suivant ses desseins, pour atteindre le maximum des
buts qu'il s'est fixé, sans confondre son action avec celle des partis qui
prétendent, eux aussi, transformer l'ordre politique et social, et préconisent
pour cela la dictature prolétarienne et la constitution d'un Etat soi-disant
provisoire.

En dehors de cette action essentielle, le congrès déclare que, par son action
revendicative quotidienne, le syndicalisme poursuit la coordination des efforts
ouvriers, l'accroissement du mieux-être des travailleurs par la réalisation
d'améliorations immédiates, telles que : la diminution des heures de travail,
l'augmentation des salaires, etc.

Il prépare chaque jour l'émancipation des travailleurs qui ne sera réalisée
que par l'expropriation du capitalisme.

::

    Lyon, 1er & 2 novembre 1926
