.. index::
   Chartes

.. _chartes:

===============================
Chartes CNT
===============================

.. toctree::
   :maxdepth: 4

   amiens/amiens
   lyon/lyon
   paris/paris
