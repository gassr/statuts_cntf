
.. index::
   pair: Historique ; Motions adoptées

.. _histo_adoptees_cnt:

===========================================================
Historique des motions adoptées (résolutions) 1993-2012
===========================================================




Présentation
============

Le Congrès confédéral décide l'élaboration d'un recueil des résolutions
confédérales en vigueur pré - titré « CNT - Fonctionnement &
Orientations ».

Cet ouvrage annule et remplace le « recueil des motions en vigueur ».

A l'issue de chaque Congrès, le recueil des résolutions confédérales en vigueur
devra être mis à jour par le bureau confédéral ou par voie de délégation sous
sa responsabilité.

Les modifications attendues intégreront les nouvelles dispositions et orientations
adoptées.

Le recueil des résolutions confédérales en vigueur est un ouvrage synthétique
des différentes motions adoptées.

L'effort de synthèse portera sur l'intégration des décisions les plus récentes,
invalidant, supprimant, voire complétant par voie de conséquence les décisions
antérieures.

Le recueil des résolutions confédérales en vigueur est structuré selon deux
axes principaux :
- Fonctionnement
- et Orientations.

Chacun de ces axes comprend plusieurs catégories thématiques afin de favoriser
tant que possible la lisibilité de l'ouvrage.

Chaque résolution sera annotée d'un historique des différents Congrès confédéraux
ayant apporté des modifications.

Afin de conserver l'historique des différentes décisions de Congrès Confédéraux,
**l'intégralité des motions adoptées est portée en annexe selon la même
classification, annotées des Congrès Confédéraux concernés**.

Une fois l'ouvrage mis à jour dans **des délais raisonnables**, il est porté à
connaissance des syndicats par le Bureau Confédéral.

Le CCN le plus proche valide l'ouvrage sur avis du Bureau Confédéral.

Historique
==========

.. toctree::
   :maxdepth: 3

   2014/2014
   2012/2012
