


=====================================
Motions Stratégie syndicale de la CNT
=====================================


- :ref:`strategie_syndicale_cnt_congres_lille_2008`
- :ref:`motions_strategie_syndicale_cnt_congres_agen_2006`
- :ref:`motions_strategie_syndicale_cnt_congres_saint_denis_2004`
- :ref:`motion_strategie_syndicale_cnt_congres_toulouse_2001`
- :ref:`motion_strategie_syndicale_cnt_congres_paris_1998`
- :ref:`motions_strategie_syndicale_cnt_congres_lyon_1996`
