

=======================
Motions "International"
=======================

- :ref:`motions_international_cnt_congres_lille_2008`
- :ref:`motions_international_cnt_congres_agen_2006`
- :ref:`motions_international_cnt_congres_toulouse_2001`
- :ref:`motions_international_cnt_congres_paris_1998`
- :ref:`motion_international_cnt_congres_paris_1997`
- :ref:`motions_international_cnt_congres_lyon_1996`
