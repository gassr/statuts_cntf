

================================================================
Motions Travail, revendications salariales et protection sociale
================================================================



Revendications salariales
=========================

- :ref:`motion_protection_sociale_cnt_congres_agen_2006`


Critique du revenu garanti universel
====================================

- :ref:`motion_protection_sociale_cnt_congres_saint_denis_2004`
