.. index:
   pair: Orientations ; CNT
   ! Orientation

.. _motions_orientation_cnt:

==============================
Motions Orientations de la CNT
==============================

.. toctree::
   :maxdepth: 4


   identite
   strategie_syndicale
   solidarite_confederale
   travail_revendications_salariales
   precarite_et_reduction_temps_de_travail
   travailleurs_immigres
   antifascisme
   droits_des_femmes
   economie
   ecologie
   antifascisme
   antimilitarisme
   international
