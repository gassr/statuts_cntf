

==================================================
Motions Précarité et réduction du temps de travail
==================================================



Création d’une Commission nationale chômeurs et précaires
=========================================================

- :ref:`motion_precarite_cnt_congres_lille_2008`

Sur la negociation de la duree et de l’organisation du travail
==============================================================

- :ref:`motion_precarite_cnt_congres_paris_1998`

RTT et precarité
================

- :ref:`motion_precarite_cnt_congres_lyon_1996`
