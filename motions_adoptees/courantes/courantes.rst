
.. index::
   pair: Motions; Recueil des résolutions confédérales en vigueur depuis 1993
   ! Motions adoptées (1993-2010)

.. _recueil_motions_adoptees_cnt_1993_2010:

===========================================================
Recueil des motions adoptées (résolutions) depuis 1993
===========================================================

.. seealso:: :ref:`motion_recueil_des_resolutions_confederales_lille_2008`

Le :ref:`Congrès confédéral de Lille <congres_CNT_2008_lille>` décide l'élaboration d'un recueil des résolutions
confédérales en vigueur pré-titré **CNT - Fonctionnement et pré-Orientations**.

Cet ouvrage annule et remplace le "recueil des motions en vigueur".

A l'issue de chaque Congrès, le recueil des résolutions confédérales en vigueur
devra être mis à jour par le :term:`bureau confédéral` ou par voie de délégation
sous  sa responsabilité.

Les modifications attendues intégreront les nouvelles dispositions et
orientations adoptées.

**Le recueil des résolutions confédérales en vigueur est un ouvrage synthétique
des différentes motions adoptées.**

L'effort de synthèse portera sur l'intégration des décisions les plus récentes,
invalidant, supprimant, voire complétant par voie de conséquence les décisions
antérieures.

Le recueil des résolutions confédérales en vigueur est structuré selon deux
axes principaux

- :ref:`motions_fonctionnement_cnt`
- :ref:`motions_orientation_cnt`

Chacun de ces axes comprend plusieurs catégories thématiques afin de favoriser
tant que possible la lisibilité de l'ouvrage.

Chaque résolution sera annotée d'un historique des différents Congrès
confédéraux ayant apporté des modifications.

Afin de conserver l'historique des différentes décisions de Congrès Confédéraux,
l'intégralité des motions adoptées est portée en annexe selon la même
classification, annotées des Congrès Confédéraux concernés.

Une fois l'ouvrage mis à jour dans des délais raisonnables, il est porté à la
connaissance des syndicats par le Bureau Confédéral.

Le :term:`CCN` le plus proche valide l'ouvrage sur avis du Bureau Confédéral.


::

    30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
    5ème Mise à jour Février 2010 – Réalisée par Emmanuel LEBEAUX
    (CNT ETPIC Nîmes & Gard sud) & ROUSSEAU Gaëtan (ETPRECI 35)


.. toctree::
   :maxdepth: 4


   orientations/orientations
   fonctionnement/fonctionnement
