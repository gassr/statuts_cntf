
.. _motions_tresorerie_confederale_cnt:

==============================
Motions Trésorerie confédérale
==============================


- :ref:`motions_tresorerie_confederale_cnt_congres_lille_2008`
- :ref:`motions_tresorerie_confederale_cnt_congres_agen_2006`
- :ref:`motion_tresorerie_confederale_cnt_congres_saint_denis_2004`
- :ref:`motion_tresorerie_confederale_cnt_congres_lyon_1996`
