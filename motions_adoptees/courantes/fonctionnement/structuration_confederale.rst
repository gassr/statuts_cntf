

.. _motions_structuration_confederale_cnt:

=================================
Motions structuration confédérale
=================================


- :ref:`motions_structuration_confederale_cnt_congres_lille_2008`
- :ref:`motions_structuration_confederale_cnt_congres_agen_2006`
- :ref:`motion_structuration_confederale_cnt_congres_saint_denis_2004`
- :ref:`motions_structuration_confederale_cnt_congres_toulouse_2001`
- :ref:`motion_structuration_confederale_cnt_congres_1993_paris`
