
.. _motions_presse_confederale_cnt:

==========================
Motions Presse confédérale
==========================

- :ref:`motions_presse_confederale_cnt_congres_lille_2008`
- :ref:`motions_presse_2004`
- :ref:`motions_presse_confederale_cnt_congres_paris_1998`
- :ref:`motions_presse_confederale_cnt_congres_lyon_1996`
