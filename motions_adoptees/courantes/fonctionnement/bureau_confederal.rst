

.. _motions_bureau_confederal_cnt:


=========================
Motions Bureau Confédéral
=========================

- :ref:`motions_bureau_confederal_cnt_congres_agen_2006`
- :ref:`motion_bureau_confederal_cnt_congres_toulouse_2001`
- :ref:`motions_bureau_confederal_cnt_congres_lyon_1996`
