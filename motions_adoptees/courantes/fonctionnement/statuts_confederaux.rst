
.. _motions_statuts_confederaux_cnt:

===========================
Motions Statuts Confédéraux
===========================

- :ref:`motions_statuts_confederaux_cnt_congres_saint_denis_2004`
- :ref:`motions_statuts_confederaux_cnt_congres_paris_1998`
- :ref:`motion_statuts_confederaux_cnt_congres_lyon_1996`
