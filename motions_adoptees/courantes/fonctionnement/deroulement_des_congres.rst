
.. _motions_deroulement_des_congres:

====================================
Déroulement des Congrès Confédéraux
====================================

- :ref:`motion_2_2012_sanso69`
- :ref:`motion_3_2012_ste72`
- :ref:`motion_4_2012_stel`
- :ref:`motion_5_2012_stea`
