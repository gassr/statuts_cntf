


==========================
Motions Outils confédéraux
==========================




Recueil des résolutions confédérales en vigueur
================================================

- :ref:`motion_outils_confederaux_cnt_congres_lille_2008`

Imprimerie confédérale, crèches confédérales, Acquisition d’un lieu de vie
===========================================================================

- :ref:`motion_outils_confederaux_cnt_congres_agen_2006`


Création d’une fondation de l’histoire du mouvement ouvrier
===========================================================

- :ref:`motions_outils_confederaux_cnt_congres_toulouse_2001`


Annuaire confédéral, Fête confédérale du Combat Syndicaliste, Bibliothèques, Formation
=======================================================================================

- :ref:`motions_outils_confederaux_cnt_congres_lyon_1996`
