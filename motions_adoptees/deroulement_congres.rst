

=========================================
Déroulement des Congrès Confédéraux & CCN
=========================================

Introduction
=============

**30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille**
67 syndicats à jour de cotisations / 47 présents

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 35         | 8        | 4          | 0                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Identification des votes
========================

En accord avec l'un de ses principes essentiels, la démocratie directe, la CNT
décide, dès maintenant, de procéder à l'identification et l'enregistrement
des votes lors de toutes ses rencontres statutaires (CCN, congrès...) et de
joindre un relevé détaillé de ceux-ci au compte-rendu de ces rencontres.

La confédération veille à se donner tous les moyens techniques nécessaires
(informatique, papier ou autres) permettant cette identification.

Validité du Congrès confédéral et modalités de vote
===================================================


**29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen**

Décisions prises suite au rapport d’une commission de travail nommée par le
Congrès, suite au rejet au vote de toutes les motions relatives aux modalités
de vote.

Le Congrès se déroule sur la base de 1 syndicat = 1 voix et les procurations
écrites sont acceptées (dispositions statutaires).

Le Congrès adopte et appliquera les modalités de vote suivantes, pour son
déroulement :

Préalables à la tenue ou la poursuite d’un Congrès Confédéral
=============================================================

Syndicats enregistrés : 64


Quorum d’ouverture du Congrès
-----------------------------

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 35         | 20       | 9          | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

Afin qu’un Congrès confédéral puisse se tenir, **il est nécessaire que 50%
minimum des syndicats à jour de cotisations soient présents**.


.. _quorum_durant_congres:

Quorum durant le Congrès
------------------------

Pour : 37

Pour que le Congrès confédéral puisse se poursuivre, **il est nécessaire que
50% minimum des syndicats présents à l’ouverture du Congrès, soient encore
présents.**

Ont été discutées : existence de motions dites *sensibles* (rejet par 43
votes contre sur 61 présents), la classification des motions par thèmes
(retenue)

Comptage des entrées et décompte des sorties définitives
--------------------------------------------------------

Syndicats enregistrés : 70 / Présents : 61

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 47         | 0        | 2          | 5                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+


Adopté.


Modalités de votes
==================

Majorité pour qu’une motion soit acceptée
Syndicats enregistrés : 70 / Présents : 61
Pour : 35 Contre : 10

+------------+----------+
| Pour       | Contre   |
+============+==========+
| 35         | 10       |
|            |          |
+------------+----------+

motion
--------

Le seuil d’admissibilité d’une motion est de 50% de votes « pour » sur la base
du nombre syndicats enregistrés au Congrès.

Les 2 propositions alternatives de majorité de la commission étaient : 50% des
présents dans la salle au moment des votes, et 50% des voix exprimés (pour,
contre, et abstention – les PAV ne sont pas pris en compte)

Intégration des modalités de vote et des règles de quorum aux règles organiques
associées aux statuts confédéraux

Syndicats enregistrés : 70 / Présents : 60

+------------+----------+------------+---------------------------+
| Pour       | Contre   | Abstention | Ne prend pas part au vote |
+============+==========+============+===========================+
| 45         | 2        | 7          | 6                         |
|            |          |            |                           |
+------------+----------+------------+---------------------------+

**Adopté.**
