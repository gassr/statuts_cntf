
.. index::
   pair: Motions; Recueil des résolutions confédérales en vigueur depuis 1993
   pair: Motions; Adoptées
   ! Motions adoptées

.. _recueil_motions_adoptees_cnt:

===========================================================
**Recueil des motions adoptées par la CNT**
===========================================================

- :ref:`motion_recueil_des_resolutions_confederales_lille_2008`


Motions au format PDF (mis à jour juin 2015)
=============================================

Au-delà des comptes-rendus de Congrès, le **Recueil des motions de Congrès
adoptées** constitue un ouvrage *interne* (pourquoi seulement interne ? NDLR)
de référence quant aux résolutions adoptées par les Congrès confédéraux successifs.

Véritable outil de **transparence démocratique**, il tend à favoriser la
mémoire interne et une meilleure connaissance de tous et toutes des
résolutions prises pour et par la CNT.

Son actualisation régulière contribue donc à enrichir l’évolution du débat
et la compréhension du projet anarcho-syndicaliste etsyndicaliste
révolutionnaire.

A l'issue de chaque Congrès, le **Recueil des motions de Congrès adoptées**
devra donc être mis à jour par le bureau confédéral oupar voie de délégation
sous sa responsabilité.

Le recueil est structuré selon deux axes principaux:

- Fonctionnement & Orientations, secondés d’un classement thématique identifiable
  en sommaire afin de favoriser tant que possible la lisibilité de l'ouvrage.

La date de la mise à jour est indiquée en page de garde.

Chaque motion figurant au recueil dispose d’un titre lui-même précédé
d’un chapeau comportant le Congrès confédéral correspondant et
l’identification des votes (nom des syndicats votants et résultat
numérique des votes).

La mise à jour est réalisée dans les meilleurs délais.

Le CCN le plus proche valide l'ouvrage sur avis du Bureau Confédéral.

Il devraêtre porté à la connaissance des syndicats par le Bureau Confédéral
au moins 6 mois avant le Congrès suivant, en format papier ou électronique
(diffusion via la [listesyndicats]).

Le « Recueil des motions de Congrès adoptées » est par ailleurs transmis
gratuitement par le Bureau Confédéral à tous les syndicats.

Ilest par ailleurs systématiquement proposé à tout nouveau syndicat
labellisé par la CNT.


Réalisation initiale: LEBEAUX Emmanuel (ETPIC Gard sud), LEHBERGER Tania (STICS 13),
SAMOUTH Théo (SIRN 44)

Mise à jour 2015: ETIENNE Aurélien (Secrétariat Confédéral, STE 57)

:download:`Télécharger les motions 1993-2014 au format PDF mise à jour juin 2015 <Recueil_des_motions_de_congres_adoptees_1993_2014.pdf>`


Description
===========

Le :ref:`Congrès confédéral de Lille <congres_CNT_2008_lille>` décide l'élaboration d'un recueil des résolutions
confédérales en vigueur pré-titré **CNT Fonctionnement et pré-Orientations**.

Cet ouvrage annule et remplace le **recueil des motions en vigueur**.

A l'issue de chaque Congrès, le recueil des résolutions confédérales en vigueur
devra être mis à jour par le :term:`bureau confédéral` ou par voie de délégation
sous sa responsabilité.

Les modifications attendues intégreront les nouvelles dispositions et
orientations adoptées.

**Le recueil des résolutions confédérales en vigueur est un ouvrage synthétique
des différentes motions adoptées.**

L'effort de synthèse portera sur l'intégration des décisions les plus récentes,
invalidant, supprimant, voire complétant par voie de conséquence les décisions
antérieures.

Le recueil des résolutions confédérales en vigueur est structuré selon deux
axes principaux

- :ref:`motions_fonctionnement_cnt`
- :ref:`motions_orientation_cnt`

Chacun de ces axes comprend plusieurs catégories thématiques afin de favoriser
tant que possible la lisibilité de l'ouvrage.

Chaque résolution sera annotée d'un historique des différents Congrès
confédéraux ayant apporté des modifications.

Afin de conserver l'historique des différentes décisions de Congrès Confédéraux,
l'intégralité des motions adoptées est portée en annexe selon la même
classification, annotées des Congrès Confédéraux concernés.

Une fois l'ouvrage mis à jour dans des délais raisonnables, il est porté à la
connaissance des syndicats par le Bureau Confédéral.

Le :term:`CCN` le plus proche valide l'ouvrage sur avis du Bureau Confédéral.


::

  30ème Congrès confédéral des 19,20 et 21 septembre 2008 à Lille
  5ème Mise à jour Février 2010 - Réalisée par Emmanuel LEBEAUX
  (CNT ETPIC Nîmes & Gard sud) & ROUSSEAU Gaëtan (ETPRECI 35)


:download:`Télécharger les motions 1993-2014 au format PDF mise à jour juin 2015 <Recueil_des_motions_de_congres_adoptees_1993_2014.pdf>`


Déroulement des congrès
========================

.. toctree::
  :maxdepth: 4


  deroulement_congres


Motions courantes
=================

.. toctree::
  :maxdepth: 4

  courantes/courantes

Historique des motions
========================

.. toctree::
  :maxdepth: 4

  historique/historique
