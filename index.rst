
.. index::
   ! Statuts Confédéraux

.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center


.. _cnt_statuts:
.. _statuts_confederaux:
.. _regles_confederales:

=============================================
**Statuts confédéraux de la CNT-F**
=============================================

- http://cnt-f.org
- :ref:`congres_cnt_f`
- :ref:`glossaire_cnt`
- :ref:`glossaire_juridique`
- :ref:`genindex`


.. toctree::
   :maxdepth: 4

   index/index
   chartes/chartes
   CNT/CNT
   motions_adoptees/motions_adoptees
   meta/meta
