
.. index::
   pair: Article ; 1
   pair: Statuts ; CNT 2010


.. _titre_1_statuts_cnt_2010:
.. _article_premier_statuts_CNT_2010:

================================
TITRE I Article 1 But de la CNT
================================


La Confédération Nationale du Travail a pour but :

- De grouper, sur le terrain spécifiquement économique, sans autre forme de
  discrimination, pour la défense de leurs intérêts matériels et moraux, tous
  les travailleurs/ses à l'exception des employeurs/ses et des forces
  répressives de l'Etat considéréEs comme des ennemiEs des travailleurs/ses.
- De poursuivre, par la lutte de classes et l'action directe, la libération des
  travailleurs/ses qui ne sera réalisée que par la transformation totale de la
  société actuelle.

Elle précise que cette transformation ne s'accomplira que par:

- la suppression du salariat et du patronat,
- la syndicalisation des moyens de production, de répartition, d'échange et de
  consommation,
- le remplacement de l'Etat par un organisme issu du syndicalisme lui-même et
  géré par l'ensemble de la société.

La Confédération Nationale du Travail reposant sur le producteur/trice, garantit
à celui/celle-ci la direction de l'organisation des travailleurs/ses.

Elle est indépendante de tout type d'organisation politique, religieuse ou autre;
ce qui implique que tout adhérentE ne peut agir à la CNT au nom d'autres
organisations.

La CNT, préconisant l'internationalisme comme moyen d'émancipation,
collabore à l'étude des questions sociales et économiques et œuvre à la
libération des travailleurs, à l'échelle internationale.

La CNT développe la culture, l'instruction et la conscience de classe des
travailleurs/ses et entretient la solidarité parmi eux/elles.
