

.. _article_25_statuts_CNT_2010:

========================
TITRE VI Article 25 2010
========================

Tout cas litigieux non prévu sera soumis à la plus prochaine réunion du C.C.N.,
et tranché selon l'esprit des présents statuts.
