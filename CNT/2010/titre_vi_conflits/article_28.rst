
.. _article_28_statuts_CNT_2010:

====================================
TITRE VI Article 28 Dissolution
====================================

En cas de dissolution, la liquidation de l’actif social sera versée à
l’Internationale à laquelle adhère la CNT ou, à défaut, à une ou plusieurs
organisations syndicalistes révolutionnaires et/ou anarcho-syndicalistes du
même pays ou d’un autre pays.
