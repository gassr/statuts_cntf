

.. _article_23_statuts_CNT_2010:
.. _article_22_caisse_internationale_2010:

=================================
Article 23 Caisse internationale
=================================

La cotisation destinée à l’organisation syndicale internationale à laquelle
adhère la Confédération ou, à défaut, à l’internationalisme est représentée
par un timbre trimestriel obligatoire.

Le montant de cette cotisation est fixé par les Congrès internationaux ou, à
défaut, par le Congrès confédéral. Le montant de ces timbres est inscrit à un
compte spécial **International**.
