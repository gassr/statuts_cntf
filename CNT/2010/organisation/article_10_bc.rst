

.. _article_10_statuts_CNT_2010:

========================================
Article 10 Rapport Bureau Conféral (BC)
========================================

Le Bureau doit adresser semestriellement, avant chaque CCN, un rapport
d’activité aux Syndicats, et obligatoirement, avant chaque Congrès.
