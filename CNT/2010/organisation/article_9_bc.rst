
.. _article_9_statuts_CNT_2010:

==================================================================
Article 9 désignation de déléguéEs par le Bureau Confederal `B.C`
==================================================================

Entre deux CCN, la désignation des déléguéEs de la C.N.T. aux diverses
commissions, comités ou conseils extérieurs à la C.N.T. est faite par
le :term:`B.C`

Ces déléguéEs aviseront le :term:`B.C` des convocations qui pourraient
leur parvenir.

Ils seront tenus de demander un mandat au :term:`B.C` sur l'objet de leur
convocation.

Ils auront à **rendre compte de son accomplissement dans la forme que
le :term:`B.C` leur demandera**.
