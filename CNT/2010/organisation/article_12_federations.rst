
.. index::
   pair: Fédération; Statuts

.. _article_12_statuts_CNT_2010:

=======================
Article 12 Fédérations
=======================

Chaque Fédération constitue un bureau.

En plus du rôle technique qui leur incombe et qui est du plus haut
intérêt, dont l’aide à la création de nouveaux Syndicats, les
Fédérations ont pour mission de coordonner l’action de leurs Syndicats.
