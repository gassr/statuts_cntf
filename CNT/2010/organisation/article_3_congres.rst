
.. index::
   pair: Statuts CNT 2010; Article 3


.. _article_3_statuts_CNT_2010:

==============================
Article 3 Décision de Congrès
==============================

La C.N.T. est administrée suivant les directives données et les décisions
prises par les Syndicats réunis en Congrès, **tous les deux ans**.

**A la CNT, le pouvoir appartient aux Syndicats, cellule de base de la
Confédération, et à leurs adhérents au sein des syndicats.**
