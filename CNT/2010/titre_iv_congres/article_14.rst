
.. _article_14_statuts_CNT_2010:

===========================================
TITRE IV Article 14 préparation des Congrès
===========================================

.. seealso::

   - :ref:`fonctionnement_congres_confederal`


Le Bureau confédéral avise les Syndicats de la tenue du Congrès **sept mois
avant la date prévue afin que les Syndicats qui le souhaitent puissent
proposer des modifications de Statuts et inscrire les points à l’ordre du jour**.

Il dresse l’ordre du jour d’après les réponses des Syndicats.

Il établit lui-même le rapport moral et le rapport financier, ainsi que les
projets sur des réalisations pratiques s’il y a lieu et si les Syndicats ne
les ont pas eux-mêmes évoqués.

Il transmet ces rapports à tous les Syndicats.

Le Syndicat qui a demandé l’inscription d’un point à l’ordre du jour établit
lui-même le rapport ou la motion sur ce point.

Les rapports et motions inscrits à l’ordre du jour définitif sont tirés et
envoyés par le Bureau à tous les Syndicats, **trois mois avant la date du Congrès.**


**Les contre-propositions, amendements et contributions sur l’ordre du jour
établi doivent être impérativement connus des Syndicats au moins un mois
avant la date du Congrès**.

Passée cette date, le Congrès peut en refuser leur discussion.

Quelle que soit la nature du Congrès, ordinaire ou extraordinaire, les points
proposés à l’ordre du jour ne peuvent être acceptés que s’ils sont accompagnés
de motions.
