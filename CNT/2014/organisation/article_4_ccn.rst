
.. index::
   pair: C.C.N ; Statuts


.. _article_4_statuts_CNT_2014:
.. _article_4_ccn_2014:

==============================================
Article 4 : Comité Confédéral National (C.C.N)
==============================================


Dans l'intervalle des Congrès, la C.N.T. est administrée par le C.C.N.

Le CCN est constitué par une délégation de chaque Union régionale
existante.

Il se réunit à intervalle régulier, **au moins trois fois entre deux congrès**,
et extraordinairement, en cas de circonstances graves, sur la décision du
Bureau Confédéral ou à la demande de trois Unions régionales.

Chaque région a une voix.

Il assure la continuité des décisions adoptées en Congrès et supervise la
gestion exécutive du Bureau confédéral et de la Commission Administrative.

**Le CCN ne peut contrevenir aux décisions de Congrès confédéral**.

Les membres du Bureau, et une délégation de chaque Fédération siègent
à titre consultatif, ainsi que les membres de la C.A.

Le Bureau confédéral avise les Unions Régionales de la tenue du
prochain CCN, trois mois avant la date dudit CCN. Il sollicite
leurs propositions d’ordre du jour et l’élabore en collaboration
avec la C.A., en y ajoutant ses préoccupations administratives.

Les Syndicats sont informés au moins un mois avant la tenue du
CCN de l’ordre du jour définitif.
