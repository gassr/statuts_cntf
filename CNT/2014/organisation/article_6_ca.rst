
.. index::
   ! CA
   ! Commission Administrative
   pair: Commission ; Administrative

.. _article_6_statuts_2014:
.. _article_6_statuts_CNT_2014:
.. _article_6_statuts:

=================================================
Article 6 Commission Administrative (C.A.) 2014
=================================================



Texte
======

Dans l'intervalle des Comités confédéraux nationaux, la C.N.T. est
administrée par le :term:`Bureau Confédéral` (B.C) et
la :term:`Commission Administrative` (C.A.)  élus par le Congrès.

**La C.A. est composée des différents secrétariats confédéraux mis
en place par le congrès**.

Les membres de la CA ne pourront occuper aucun poste responsable relevant
d'une **organisation politique** (**y compris toute sorte de groupement
se présentant à des élections - hors élections professionnelles**), d'une
secte philosophique ou religieuse.

La nouvelle C.A. entre en fonction à l'issue du Congrès.
Les membres de la C.A. sortante sont immédiatement rééligibles.

La C.A. doit coordonner son action et agir au travers des différents
secrétariats qui la composent conformément aux accords de Congrès et de CCN.


Explications
=============

.. seealso::

   - :ref:`article_6_statuts_2012`
   - :ref:`motion_18_2014_sinr44`

Modification de :ref:`l'article 6 des statuts <article_6_statuts_CNT_2012>` (en gras)

Les membres de la CA ne pourront occuper aucun poste responsable relevant
d'une **organisation politique** (**y compris toute sorte de groupement se
présentant à des élections - hors élections professionnelles**), d'une
secte philosophique ou religieuse
