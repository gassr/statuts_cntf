
.. index::
   pair: Article; 16


.. _article_16_statuts_CNT_2014:
.. _article_16_statuts_CNT:

===========================================================
TITRE IV – Article 16 Voix des syndicats au Congrès (2014)
===========================================================

.. seealso::

   - :ref:`fonctionnement_congres_confederal`
   - :ref:`article_16_statuts_CNT_2014`


Ancien texte
=============

Chaque Syndicat représenté au Congrès dispose d'une voix.

Chaque déléguéE ne peut représenter exceptionnellement
que **trois Syndicats  au maximum**.

Un membre du Bureau ou de la :term:`C.A.` ne peut représenter que son Syndicat.
Il ne peut détenir un mandat d'un autre Syndicat.

**Les membres de la C.A. assistent à titre consultatif, au Congrès**,
ainsi qu'un représentant de chaque Fédération d'industrie.


Nouveau texte
==============

Chaque Syndicat représenté au Congrès dispose d'une voix.

Chaque syndicat présent physiquement au congrès ne peut représenter
qu'un seul autre syndicat.

Un membre du Bureau ou de la :term:`C.A.` ne peut représenter que son
Syndicat.
Il ne peut détenir un mandat d'un autre Syndicat.

**Les membres de la C.A. assistent à titre consultatif, au Congrès**,
ainsi qu'un représentant de chaque Fédération d'industrie.


Explications
=============

.. seealso::

   - :ref:`motion_3_2014_sinr44`


Modification de l'article 16 des statuts, la phrase::

    Chaque déléguéE  ne peut représenter exceptionnellement que trois
    Syndicats au maximum.

est remplacée par::

	Chaque syndicat présent physiquement au congrès ne peut représenter
	qu'un seul autre syndicat.

Un syndicat donnant procuration doit le signaler lui-même, soit à la
commission d'organisation du congrès (avant le congrès), soit au congrès
directement (en cas de départ avant la fin du congrès).

Un syndicat qui quitte le congrès en cours peut donner procuration à un
autre syndicat, à condition que ce dernier ne détienne pas déjà une
procuration.
En outre, sont uniquement prises en compte les procurations pour les
votes des motions, contre-motions et amendements déposés dans les délais
préalablement au Congrès.
Lorsqu'une motion de synthèse ou une motion modifiée au cours du Congrès
lui-même est portée au vote, seules les voix des syndicats physiquement
présents sont comptabilisées.

Si c'est pour des raisons financières qu'un syndicat ne peut pas venir
au congrès confédéral, le trésorier de ce syndicat prend contact avec
la trésorerie confédérale qui voit comment aider le syndicat à se rendre
au congrès (prise en charge totale, partielle ou échéanciers, etc).
