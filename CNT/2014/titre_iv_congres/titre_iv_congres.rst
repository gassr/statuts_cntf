

.. _congres_statuts_CNT_2014:
.. _congres_statuts_CNT:


==================
TITRE IV – Congrès
==================

.. seealso::

   - :ref:`fonctionnement_congres_confederal`

.. toctree::
   :maxdepth: 3

   article_13
   article_14
   article_15
   article_16
