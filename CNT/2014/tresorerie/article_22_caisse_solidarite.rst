
.. _caisse_de_solidarite:
.. _article_22_statuts_CNT_2014:
.. _article_22_caisse_solidarite:

===============================
Article 22 Caisse de solidarité
===============================

II est institué une Caisse confédérale dite de solidarité, dont le montant est
destiné à venir en aide aux travailleurs/ses victimes de la lutte sociale.

Cette Caisse est alimentée par les timbres solidarité et la vente des cartes.

Deux timbres par an sont obligatoires.

**Chaque syndiquéE peut en prendre facultativement autant qu'il lui plaît**.

Le montant du timbre solidarité est fixé par le Congrès.

Les fonds sont inscrits au compte **Caisse de Solidarité**.
