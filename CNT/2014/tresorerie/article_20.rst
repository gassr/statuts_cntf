

.. _article_20_statuts_CNT_2014:

====================================================================
Article 20 Caisse de la CNT, trésorierE confédéralE , compte-rendus
====================================================================

La Caisse de la C.N.T. est confiée au/à la trésorierE confédéralE qui en est
responsable sous le contrôle du B.C..

La nature des dépenses est contrôlée par le Congrès et un compte rendu
financier sera fait à chaque C.C.N. par le/la trésorierE confédéralE.

Ce compte rendu évoque également les opérations de gestion de la Caisse de
solidarité.
