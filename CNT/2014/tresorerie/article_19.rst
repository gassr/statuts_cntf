
.. _article_19_statuts_CNT_2014:

==============================
Article 19 commande des cartes
==============================

Chaque Syndicat, passe commande au/à la trésorierE confédéralE de la quantité
évaluée de cartes et timbres pour une période de six mois.

Cette commande transite par les trésorierEs de l’Union locale ou de l’Union
régionale dont est membre le Syndicat.

Trimestriellement, le Syndicat s’acquitte des cotisations au Bureau confédéral,
aux bureaux exécutifs de sa Fédération, son Union locale et son Union
régionale, pour la quantité de cotisations reçues par les adhérents du Syndicat.

Le montant des cotisations étant fixé par les congrès respectifs au niveau de
l’aire géographique ou d’industrie concernée.

En fin d’année, les timbres qui portent la mention de leur millésime
d’utilisation, doivent être réglés ou rendus au Bureau confédéral.

La CNT est habilitée à recevoir toute aide matérielle ou financière extérieure,
à partir du moment où n’existe aucune contre partie d’engagement de toute sorte
et n’est pas remise en question son indépendance.
