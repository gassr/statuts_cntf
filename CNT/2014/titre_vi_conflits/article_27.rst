
.. _article_27_statuts_CNT_2014:

=================================================
TITRE VI Article 27 modification des statuts
=================================================

Les présents Statuts ne peuvent être modifiés que par un Congrès ordinaire,
à condition que le texte des modifications ait été porté à la
connaissance des Syndicats **six mois avant la date du Congrès**.

Les motions dites de ``règles organiques`` peuvent être présentés dans les
mêmes délais que les motions ordinaires, mais devront être présentées avec
cette mention spécifique en en-tête.
