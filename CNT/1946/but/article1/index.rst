
.. _article_1_statuts_1946:

===============================
Article 1
===============================

La Confédération Nationale du Travail a pour but:

- De grouper, sur le terrain spécifiquement économique, pour la défense de
  leurs intérêts matériels et moraux, tous les salariés, à l’exception des
  forces répressives de l’Etat, considérées comme des ennemies des travailleurs.

- De poursuivre, par la lutte de classes et l’action directe, la libération des
  travailleurs qui ne sera réalisée que par la transformation totale de la
  société actuelle.

- Elle précise que cette transformation ne s’accomplira que par la suppression
  du salariat, par la syndicalisation des  moyens de production, de répartition,
  d’échange et de consommation, et le remplacement de l’Etat par un organisme
  issu du syndicalisme lui-même et géré par l’ensemble de la société.

- La Confédération Nationale du Travail reposant sur le producteur, garantit à
  celui-ci la direction de l’organisation des travailleurs.

- La Confédération Nationale du Travail fait partie intégrante de l’A.I.T.
  au sein de laquelle elle collabore à l’étude des questions sociales et
  économiques à l’échelle internationale et avec laquelle elle œuvre pour la
  libération totale des travailleurs.

- La C.N.T. créera ou placera sous son contrôle toute œuvre susceptible de
  développer l’instruction et la conscience de classe de ses adhérents,
  d’ entretenir la solidarité parmi eux et de resserrer les liens de fraternité
  qui les unissent.
