
.. _article_2_statuts_1946:

============
Article 2
============


La Confédération Nationale du Travail est constituée par:

1. Les Syndicats, groupés dans les Unions Locales et Régionales, et les
   Fédérations d’industrie ;
2. Les Unions Régionales de syndicats ;
3. Les Fédérations d’industrie.

Cette association est conçue et organisée sur des bases fédéralistes.
Nul Syndicat ne peut faire partie de la C.N.T. s’il n’adhère pas à la
Fédération d’industrie, à son Union Locale et à son Union Régionale.
Les organisations adhérentes à la C.N.T. ont droit à la marquedistinctive
appelée label confédéral.
