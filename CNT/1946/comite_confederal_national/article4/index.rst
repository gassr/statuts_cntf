
.. _article_4_statuts_1946:

===============================
Article 4
===============================

Dans l’intervalle des congrès la C.N.T. est administrée par le Comité
Confédéral National.

Le C.C.N. est constitué par un délégué de chaque Union Régionale.
Il se réunit pendant le dernier mois de chaque trimestre et extraordinairement,
en cas de circonstances graves, sur la décision de la Commission Administrative
ou à la demande de trois Unions Régionales. Chaque Région a une voix.

Les membres du Bureau, un délégué de chaque Fédération et un responsable de la
commission de contrôle siègent à titre consultatif, ainsi que les membres de
la C.A.

Les frais de délégation occasionnés par la tenue du C.C.N. sont remboursés par
la caisse du congrès de la C.N.T., dans les conditions prévues à chaque C.C.N.
