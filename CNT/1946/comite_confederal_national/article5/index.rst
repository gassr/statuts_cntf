
.. _article_5_statuts_1946:

===============================
Article 5
===============================

Le C.C.N. désigne le nombre d’employés (traducteurs, sténos, dactylos, etc.),
nécessaires au bon fonctionnement de la C.N.T., et fixe leurs appointements.

En cas d’urgence, ce nombre peut être augmenté par la C.A. sous réserve de
ratification par le C.C.N. suivant.

Les procès-verbaux de chacune des séances du C.C.N. donneront le nom des
régions représentées, excusées et absentes.

Les délégués sont tenus de rendre compte des discutions de ces divers comités
à leurs mandants.
