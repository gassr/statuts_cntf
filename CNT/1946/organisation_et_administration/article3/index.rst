
.. _article_3_statuts_1946:

==============================
Article 3
==============================


La Confédération Nationale du Travail est administrée suivant les directives
données et les décisions prises par les Syndicats réunis en Congrès,
à l’automne, tous les deux ans.
