
.. index::
   pair: Règles organiques ; 2012



.. _reactu_regles_organiques_2012:

=====================================================
Réactualisation des règles organiques de la CNT 2012
=====================================================




Règles organiques 2012
=======================


:download:`regles_organiques_reactualisation_2012.pdf`

Suppression
============

- Suppression des dispositions «Coordination des syndicats interco»
  (Congrès de Toulouse 2001)
- Suppression des dispositions liées à «Annuaire confédéral des membres du BC»
  Déjà inscrit au recueil des résolutions confédérales en vigueur
- Suppression des dispositions liées aux « Secrétariat de propagande électronique » -
  Déjà inscrit au recueil des résolutions confédérales en vigueur
- Suppression des dispositions liées à « Structuration interne du Secrétariat
  International», et à « l’intégration des syndicats dans la démarche internationale»
  Déjà inscrit au recueil desrésolutions confédérales en vigueur
- Suppression des dispositions liées à « Bilan d’activité du Bureau confédéral
  & Organigramme de la CNT » - Déjà inscrit au recueil des résolutions confédérales
  en vigueur

Ajout
======

- Intégration des nouvelles dispositions liées à la labellisation et à la
  dé-labellisation d’un syndicat (Congrès de Metz 2012)
- Ajout des dispositions liées à « Double affiliation syndicale ou double carte »
  (Congrès de Metz 2012)
- Ajout des dispositions liées à la « Composition des unions régionales »
  (Congrès de 2010 – Saint-Etienne) & suppression des dispositions liées à la
  « Composition des unions régionales » (Congrès de Paris de 1993)
- Ajout des dispositions liées aux « Compétences du Comité Confédéral (C.C.N.) »
  (Congrès de Paris de 1993) & suppression du paragaphe sur le composition des
  zones invalidé par les dispositions liées à « Composition des unions régionales »
  (Congrès de 2010 – Saint-Etienne)


Modifications
=============

- Remplacement des dispositions liées à «Organisation du Bureau confédéral et
  de la Commission administrative de la CNT » (Congrès de Lyon 1996) par
  « Structuration du Bureau confédéral et de la Commission administrative de la CNT »
  (Congrès de Metz 2012)
- Ajout des « Modalités de vote lors des Congrès confédéraux » & « Non
  comptabilisation des NPPV » (Congrès de Metz 2012)
- Ajout de « Péréquation des frais de déplacements » (Congrès de 2010 – Saint-Etienne)


Changements
===========

- Transfert de la motion « Frais de délégation » (Congrès extraordinaire de
  Lille 2011) au recueil des motions invalidée par la motion « Péréquation des
  frais de déplacements » (Congrès de 2010  Saint-Etienne)


.. _statuts_et_regles_organiques_2012:

Statuts et règles organiques CNT 2012
======================================

:download:`Télécharger les statuts et règles organiques CNT 2012 <statuts_et_regles_organiques_CNT_012.pdf>`
