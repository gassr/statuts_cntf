.. index::
   pair: Règles organiques; Article 2

.. _regles_organiques_article2_CNT:

==================================================================================
TITRE II : Règles relatives aux dispositions de l’article 2 Composition de la CNT
==================================================================================

.. seealso::

   - :ref:`article_2_statuts_CNT_2010`

.. toctree::
   :maxdepth: 3

   label_confederal
   coordination_des_intercos
