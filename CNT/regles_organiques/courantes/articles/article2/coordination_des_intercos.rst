
.. index::
   pair: Coordination ; Intercos


.. _coordination_intercos:

======================================================================================
Règle relative à l'article 2 Composition de la CNT Coordination des syndicats interco
======================================================================================

27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse.

Les syndicats Interco peuvent former, dans l’esprit de ce qu’ils sont
(des syndicats d'accueil pour la création de syndicats par branche d'industrie),
une coordination nationale des syndicats Interco de manière à :

- Se coordonner afin de mener - comme le font les syndicats constitués en
  fédérations - des campagnes de développement dans des secteurs précis
  (financiers, hôtellerie, restauration, tourisme...) ;
- Etre à l'initiative de campagnes nationales sur des thématiques plus larges
  (tout en y incluant les autres syndicats de la Confédération).

Des bilans seront faits lors des CCN et devant le Congrès.
