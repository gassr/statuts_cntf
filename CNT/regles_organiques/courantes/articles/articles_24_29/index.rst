
.. index::
   pair: Féminisation ; Règles organiques


.. _titre_6_regles_organiques_2010:

==================================================================
TITRE VI : Règles relatives aux dispositions des articles 24 à 29
==================================================================

.. seealso::

   - :ref:`article_24_statuts_CNT_2010`
   - :ref:`article_25_statuts_CNT_2010`
   - :ref:`article_26_statuts_CNT_2010`
   - :ref:`article_27_statuts_CNT_2010`
   - :ref:`article_28_statuts_CNT_2010`
   - :ref:`article_29_statuts_CNT_2010`

Divers
======

Féminisation du vocabulaire des statuts confédéraux, 26ème Congrès confédéral du 5 et 6 Décembre 1998 à Paris
--------------------------------------------------------------------------------------------------------------

Afin que soit mieux pris en compte les femmes dans nos statuts, nous souhaitons
une féminisation du vocabulaire (ex : travailleurs-se(s), etc.).
