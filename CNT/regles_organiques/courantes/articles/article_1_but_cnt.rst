

========================================================================================================
TITRE I Règles relatives aux dispositions de l’article 1 syndicalisation des travailleurs independants
========================================================================================================

.. seealso::

   - :ref:`article_premier_statuts_CNT_2010`
   - :ref:`chartes_de_paris`

29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen

Le Congrès entérine le souhait formulé dans la charte de création de la
CNT: Charte du syndicalisme révolutionnaire dite **Charte de Paris**,
charte adoptée lors du congrès constitutif de la CNT les 7, 8 et 9 décembre
1946 à Paris. (Continuatrice de la charte de Lyon de la CGT SR en 1926):

**Les syndicats de la confédération procèdent à l'adhésion des travailleurs
indépendants par branches concernées, et octroient les mêmes pouvoirs
décisionnels que tous les autres travailleurs.**
