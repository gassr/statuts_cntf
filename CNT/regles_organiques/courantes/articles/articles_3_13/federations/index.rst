
.. index::
   Fédérations (règles organiques)

.. _regles_organiques_organisation_federations_CNT:

===============================================================
Règles relatives aux dispositions de l’article 12 Fédérations
===============================================================

.. seealso::

   - :ref:`article_12_statuts_CNT_2010`

Congrès extraordinaire de Décembre 2001 à Lille.

Tout Syndicat doit reverser la part fédérale correspondant à ses adhérents
ayant une Fédération de référence.
