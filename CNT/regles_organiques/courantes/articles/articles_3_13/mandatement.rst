


==================================
pouvoir décisionnel et mandatement
==================================

Issu du Congrès extraordinaire de Décembre 2001 à Lille.

Les syndicats de la CNT sont gérés collectivement par les travailleurs/ses
eux/elles-mêmes sur la base de la démocratie directe qui se réalise notamment
au travers de l’Assemblée générale souveraine du syndicat, des déléguéEs
mandatéEs, éluEs et révocables, de la rotation des mandats et de l’implication
collective dans la vie syndicale et confédérale.

Tout mandatéE n'a un certain pouvoir que défini, délégué et contrôlé par le ou
les syndicats et leurs adhérentEs, devant lesquelLEs il est responsable et
révocable ; il en est ainsi de tout mandat.

Il est de l'effort de tous/tes que la rotation des tâches et des personnes soit
une réalité.

Contrôle et revocation des mandaté(e)s
=======================================

Congrès extraordinaire de Décembre 2001 à Lille.

1. UnE mandatéE est tenuE de respecter son mandat, en plus du respect des
   Statuts de la CNT.
2. ToutE mandatéE est révocable de fait:

	- à terme échu de son mandat ;
	- à tout moment, et ce, uniquement par la/les structure/s mandantes qui
	  l'a/ont mis en place, sur sa/leur proposition et sa/leur résolution.
3. Le/la déléguéE, faisant l'objet d'une demande de révocation, a le droit et le
   devoir de s'expliquer.
4. La révocation du/de la mandatéE ne signifie pas l'exclusion de l'adhérentE
   qu'il/elle est.
