
.. index::
   pair: CCN ;limitation des pouvoirs
   pair: CCN ; ordre du jour

===============================================================================
Règles relatives aux dispositions de l’article 4 limitation des pouvoirs du CCN
===============================================================================

.. seealso::

   - :ref:`article_4_statuts_CNT_2010`

Issu du 24ème Congrès confédéral du 6 et 7 Février 1993 à Paris.

Le CCN n’est pas habilité à modifier les statuts de la CNT, ni à créer ou
modifier des règlements, ni à décider des orientations syndicales et politiques de
la CNT, ces décisions appartiennent exclusivement au Congrès.

Les décisions techniques, politiques et syndicales que le CCN peut être amené
à prendre doivent se situer dans la ligne des décisions de congrès.

Elaboration de l’ordre du jour du CCN
======================================

30ème Congrès confédéral du 19, 20, et 21 Septembre 2008 à Lille.

Chaque UR de la CNT peut déposer les points à l'ordre du jour aux CCN.
En cas de carence avérée ou d'inexistence d'UR, les syndicats sont autorisés à
déposer des points à l'ordre du jour aux CCN sous contrôle du BC.

Dans le cadre de son mandat, le BC peut de même et par lui-même inscrire des
points d'ordre du jour en lien avec les mandats confédéraux en cours (ou non
assurés), les commissions confédérales, le fonctionnement interne (notamment
fédéral), les campagnes à développer sur la base d'un argumentaire détaillé.

Perequation des frais de tranport entre UR au CCN
==================================================

30ème Congrès confédéral du 19, 20, et 21 Septembre 2008 à Lille.

En vue de plus d’équité financière, il est réalisé, avec l’aide de la trésorerie
confédérale si nécessaire, lors de chaque C.C.N une péréquation des frais
transports de chaque délégation d’Union Régionale présente.

Afin de favoriser la venue et la collégialité des délégations, cette
péréquation peut intégrer jusqu’à 2 déléguéEs mandatéEs par Union Régionale.
