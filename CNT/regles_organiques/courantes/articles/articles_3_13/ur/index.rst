
.. index::
   Unions Régionales (règles organiques)

.. _regles_organiques_organisation_UR_CNT:

====================================================================
Règles relatives aux dispositions de l’article 11 Unions Régionales
====================================================================

.. seealso::

   - :ref:`article_11_statuts_CNT_2010`

Issue du 24ème Congrès confédéral du 6 et 7 Février 1993 à Paris amendée par
le 31eme Congrès Confédéral du 10 au 12 décembre 2010 à Saint-Etienne.

Les Unions régionales sont constituées par au moins trois syndicats composés au
minimum de cinq adhérentEs chacun.

Les syndicats isolés dans les régions non constituées en UR sont rattachés à
l'une des UR les plus proches.
