
.. index::
   pair: BC ; Elections


.. _regles_organiques_elections_BC_CNT:

====================================================================================
Règles relatives aux dispositions de l’article 6 à 10 élections du Bureau Confédéral
====================================================================================

.. seealso::

   - :ref:`article_8_statuts_CNT_2010`

Congrès extraordinaire de Décembre 2001 à Lille.

1. Dans le cas où aucune liste ne serait présentée dans ces conditions, une ou
   plusieurs listes sont constituées durant le Congrès.
   Le Congrès élit le nouveau Bureau mais celui-ci doit ensuite être ratifié
   par les Syndicats, ceux-ci n’ayant pas eu préalablement connaissance des
   candidatEs choisis.
2. Dans le cas où le Bureau ne serait pas ratifié, un délai d’un mois est laissé
   pour la constitution d’autres listes soumises à référendum des syndicats, temps
   durant lequel le Bureau non ratifié assume les tâches d’un Bureau provisoire.
3. La durée de fonctionnement d’un Bureau provisoire ne peut excéder trois
   mois.
