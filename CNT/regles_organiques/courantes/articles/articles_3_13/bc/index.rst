
.. index::
   BC (règles organiques)

.. _regles_organiques_BC_CNT:

====================================================================================
Règles relatives au Bureau Confédéral
====================================================================================

.. toctree::
   :maxdepth: 4

   elections_bc
   bilan_activites
   mandates_bc
   annuaire_membres_bc
   organisation/index
