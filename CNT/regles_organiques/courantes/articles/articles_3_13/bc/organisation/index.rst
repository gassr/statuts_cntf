.. index::
   pair: BC ; Organisation

.. _regles_organiques_organisation_BC_CNT:

======================================================================
Organisation du Bureau Confédéral et de la Commission Administrative
======================================================================

25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon.

Secrétariat confédéral
======================

Réalisation des circulaires internes, soutien logistique
aux nouveaux syndicats, relations avec les syndicats, et toutes les autres tâches
qui lui sont confiées par les statuts.

Trésorerie confédérale
======================

collecte des cotisations, paiements des factures.


Secrétariat aux contacts isolés
===============================

il a à charge de répondre aux courriers de contacts isolés, parfois d'organiser
des réunions, d'envoyer de la documentation, des tracts.... Ceci en liaison
avec le secrétariat confédéral.

Secrétariat à la propagande
===========================

Gestion des besoins en matière de matériel confédéral; assurer les tirages
nécessaires, susciter la réalisation d'affiches tracts, répondre aux
propositions reçues des syndicats. Peut aussi gérer la mise en place
d'une politique éditoriale confédérale : brochures.

Secrétariat à la gestion des stocks de matériel de propagande
=============================================================

un syndicat gère les commandes de matériel, assure le retirage du matériel,
réalise les envois.

Secrétariat au bulletin intérieur (BI)
======================================

Coordonner les envois faits au BI, assurer le tirage, les envois aux syndicats
et abonnés isolés.

Secrétariat Média
=================

rédaction régulière de communiqués de presse sur les campagnes nationales ou
les actions d'importance, communiqués moins à caractère de propagande que
d'information pure relatant des faits sur les actions et positions de la C.N.T.


Secrétariat de propagande électronique
======================================

28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis.

Gère les adresses e-mails sur la base de données de l'administration CS
(trois mois gratuits) ; envoie et diffusent des informations en coordination
avec les structures confédérales (sections syndicales, commissions, syndicats,
bureaux régionaux et confédéral) ;

Réalisent un envoi régulier (p. ex. une fois par mois) vers toutes les adresses
e-mail des différentes structures de la confédération et en direction des
sympathisants (sur la base de données des "trois mois gratuits") avec le but de
mieux diffuser l'information et la propagande actuelles de la CNT.


Bilan d’activite du bureau organigramme de la CNT confederal
============================================================

29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen.

Nous demandons au bureau confédéral et à la trésorerie d'intégrer à leur bilan
fait aux syndicats un rapport sur la structuration de la Confédération, c’est à dire
sur les unions régionales et fédérations:

- Les UR constituées avec le détail des syndicats rattachés ;
- Les Fédérations constituées avec le nombre de syndicats rattachés (bilan à faire
  éventuellement au Congrès par les Fédérations elles mêmes, si le BC manque
  d'information).

Consultation des regions entre deux ccn
=======================================

Issu du 28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis.

- Toute décision devant être prise ne relevant pas des tâches purement
  techniques et ne se situant pas dans la ligne d’une action confédérale décidée en
  congrès devra, quel que soit son degré d’urgence, après consultation de
  l’ensemble des mandatés à la CA, être validée par le bureau confédéral et par le
  secrétariat de la CA concerné (ex : secrétaire international s’il s’agit d’une
  décision relevant de l’international).
- S’il n’y a pas unanimité mais qu’une forte majorité se dégage, une procédure
  express de consultation des syndicats par l’intermédiaire des Unions régionales
  est lancée par le BC, à laquelle au moins la moitié des syndicats doit avoir
  répondu pour qu’elle soit validée. L’absence de quorum ou d’une majorité claire
  entraîne la préservation du statu quo. La question est dans ce cas considérée
  comme relevant d’une décision de congrès ou de CCN et nécessitant un débat
  préalable qui est initié à cette occasion. Les régions doivent donc faire connaître
  au BC qui il doit contacter dans ces circonstances.
- Le BC, et les mandatéEs à la CA concernéEs par le point en question (ex :
  secrétaire international s’il s’agit d’une décision relevant de l’international),
  restent responsables devant le Congrès qui les a mandatéEs, des décisions qu’ils
  choisiront de prendre.


.. toctree::
   :maxdepth: 4

   international
