
.. index::
   BC (Organisation secrétariat international)


.. _regles_organiques_organisation_BC_international_CNT:

=========================================
Secrétariat aux relations internationales
=========================================

Une commission internationale regroupe autour du secrétaire international,
plusieurs compagnons, responsables par secteurs géographiques ou linguistiques.

Le budget confédéral prévoir un crédit pour les réunions ponctuelles de la
commission.


Secrétariat chargé du contact avec les DOM-TOM
===============================================

Contacts avec certaines organisations des Tom Dom.


Structuration international interne du secrétariat
===================================================

29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen.

Le secrétariat international est composé d'au moins un mandaté en charge:

- Du secrétariat : coordination interne du secrétariat international ainsi que
  rapports avec les syndicats de la CNT, participation au BC, convocations et
  compte-rendu des réunions, représentation de la CNT au niveau international ;
- Du site Internet ;
- Des publications propres au SI et du lien avec le CS ;
- Des zones géographiques suivantes : Europe, Afrique, Asie, Amérique du Sud,
  Amérique du Nord, Proche Orient/Moyen Orient et Océanie. Ces mandats sont confiés lors du
  congrès confédéral. Si des camarades veulent rejoindre le SI après un congrès,
  ils peuvent être mandatés lors d'un CCN.

Integration des syndicats dans la démarche internationale
=========================================================

29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen.

Afin que le travail international ne soit pas assumé que par des mandatés ou
des **spécialistes**, il est nécessaire de mettre à disposition des syndicats
les outils indispensables à leur mobilisation sur les questions internationales.

Pour cela, chaque réunion du SI doit donner lieu à un compte-rendu qui sera
envoyé à chaque syndicat.

De plus, le SI est en contact avec les fédérations d'industrie afin de
favoriser les rapports de solidarité et d'échange par branche et secteur
d'industrie.
