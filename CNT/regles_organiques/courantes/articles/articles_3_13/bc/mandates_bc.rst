
.. index::
   BC (mandatés)

.. _regles_organiques_mandates_BC_CNT:

=================================
Les mandatés du Bureau Confédéral
=================================

27ème Congrès confédéral du 2, 3, et 4 Mars 2001 à Toulouse.

De par leur fonction, les mandatéEs du BC représentent l'organisation vis à vis
de l'extérieur.

Ils se doivent donc:

- d'être en conformité avec les statuts confédéraux,
- d'avoir un minimum d'expérience syndicale
- et au moins une année d'adhésion.
