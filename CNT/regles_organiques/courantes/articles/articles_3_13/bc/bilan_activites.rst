
.. index::
   BC (Bilan activités)

.. _regles_organiques_bilan_activites_BC_CNT:

============================================================
Bilan d’activité du bureau
============================================================

29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen.

Nous demandons au Bureau Confédéral et à la trésorerie d'intégrer à leur bilan
fait aux syndicats un rapport sur la structuration de la Confédération, c’est
à dire sur les unions régionales et fédérations:

- Les UR constituées avec le détail des syndicats rattachés ;
- Les Fédérations constituées avec le nombre de syndicats rattachés (bilan à
  faire éventuellement au Congrès par les Fédérations elles mêmes, si le BC
  manque d'information).
