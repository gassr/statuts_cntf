
.. index::
   ! Annuaire membres du BC
   pair: BC ; Annuaire

.. _regles_organiques_annuaire_BC_CNT:

=====================================
Annuaire Confédéral des membres du BC
=====================================

25ème Congrès confédéral du 1, 2, et 3 Novembre 1996 à Lyon.

Création d'un annuaire interne sommaire des membres du BC.

**Il est nécessaire que le BC fasse parvenir ou puisse tenir à disposition
de chaque syndicat cet annuaire précisant d'une façon nominative qui s'occupe
de telle ou telle commission, de telle ou telle tache, qui a la charge de
telle ou telle fonction.**
