

.. index::
   pair: Timbre; Confédéral

.. _timbre_confederal:

=================================================================================================
Repartition du timbre confederal 30ème Congrès confédéral du 19, 20, et 21 Septembre 2008 à Lille
=================================================================================================




Timbre standard : 2,60 € avec
==============================

-0,50 € pour l’international
-0,50 € pour solidarité
-1,60 € pour la part confédérale.

Timbre précaire : 1,20 € avec
==============================

-0,30 € pour l’international
-0,30 € pour solidarité
-0,60 € pour la part confédérale.
