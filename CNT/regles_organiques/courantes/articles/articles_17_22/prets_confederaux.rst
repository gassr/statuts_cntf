
.. index::
   pair: Prêts; Confédéraux

.. _prets_confederaux:

=================
Prêts confédéraux
=================



28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis
================================================================

Des prêts financiers peuvent être accordés à des syndicats CNT ou des unions de
syndicats CNT (statutairement reconnus) par le Bureau confédéral sur la
trésorerie confédérale.

Si l’emprunt s’élève à plus de 20% de la trésorerie confédérale disponible,
l’accord du CCN le plus proche est requis. L’UR de référence, s’il elle existe,
est consultée.

La totalité des prêts accordés par le Bureau Confédéral seul ne peut engager plus
de 33% (un tiers) de la trésorerie confédérale.


29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
=======================================================

Tout prêt demandé par un syndicat doit avoir l'assentiment de son UR qui est
solidaire du remboursement des sommes prêtées en cas de difficultés ( En
l’absence d’UR, le Congrès, convient, faute de règles de laisser au BC /
CA le soin d’aviser)
