
.. index::
   pair: Caisse de grève ; règles organiques

===================================================
Règles relatives aux dispositions de l’article 22
===================================================

.. seealso::

   - :ref:`article_22_statuts_CNT_2010`

Caisse de solidarité
====================

28ème Congrès confédéral du 23 et 25 Janvier 2004 à Saint Denis.


Une caisse de grève confédérale. Comme son nom l'indique, cette caisse doit
servir uniquement aux grévistes (retenues sur salaire constatées) ceci pour que
nous nous donnions sérieusement les moyens de nous investir dans une grève
(générale ou pas), mot d'ordre central de notre confédération.

**Priorité sera donnée à ceux qui ont des bas salaires.**
