
.. index::
   pair: Règles; Comptables

.. _regles_comptables:

=================
Règles comptables
=================

Congrès extraordinaire de Décembre 2001 à Lille.

Les dépenses sont évoquées par objet.
La répartition par objet des dépenses confédérales relève du pouvoir décisionnel
des Syndicats réunis en Congrès.
