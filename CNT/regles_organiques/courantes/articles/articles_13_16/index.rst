


============================================================================================
TITRE IV : Règles relatives aux dispositions des articles 13 à 16 concernant les Congrès CNT
============================================================================================

.. seealso::

   - :ref:`article_13_statuts_CNT_2010`
   - :ref:`article_14_statuts_CNT_2010`
   - :ref:`article_15_statuts_CNT_2010`
   - :ref:`article_16_statuts_CNT_2010`


.. toctree::
   :maxdepth: 3

   validite_du_congres
   fonctionnement_et_orientations
   procedure_referendaire
   frais_de_delegation
