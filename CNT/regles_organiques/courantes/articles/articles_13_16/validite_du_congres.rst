


.. _validite_congres_confederal:
.. _modalites_vote_congres_confederal:

===================================================
Validite du congrès confédéral et modalités de vote
===================================================


29ème Congrès confédéral du 2, 3 et 4 Juin 2006 à Agen
=======================================================

Préalables à la tenue ou la poursuite d’un Congrès Confédéral:

- Quorum d’ouverture du Congrès :
  Afin qu’un Congrès confédéral puisse se tenir, il est nécessaire que 50 %
  minimum des syndicats à jour de cotisations soient présents.
- Quorum durant le Congrès :
  Pour que le Congrès confédéral puisse se poursuivre, il est nécessaire que 50%
  minimum des syndicats présents à l’ouverture du Congrès, soient encore présents.

Les entrées et sorties définitives sont décomptées.

Modalités de votes
==================

- Majorité pour qu’une motion soit acceptée :
  Le seuil d’admissibilité d’une motion est de 50% de votes « pour » sur la base
  du nombre de syndicats enregistrés au Congrès.
  30ème Congrès confédéral du 19, 20, et 21 Septembre 2008 à Lille
- Identification des votes :
  En accord avec l'un de ses principes essentiels, la démocratie directe, la CNT
  décide de procéder à l'identification et l'enregistrement des votes lors de toutes
  ses rencontres statutaires (CCN, congrès...) et de joindre un relevé détaillé de
  ceux-ci au compte-rendu de ces rencontres.
  La confédération veille à se donner tous les moyens techniques nécessaires
  (informatique, papier ou autres) permettant cette identification.
