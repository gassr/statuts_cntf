
.. index::
   pair: Fonctionnement & orientations ; Règles organiques

.. _fonctionnement_et_orientations:

=============================================================================
Création et mise à jour de l’ouvrage : "CNT-Fonctionnement & Orientations"
=============================================================================

Recueil des résolutions confédérales en vigueur

30ème Congrès confédéral du 19, 20, et 21 Septembre 2008 à Lille.

A l'issue de chaque Congrès, le recueil des résolutions confédérales en vigueur
devra être mis à jour par une commission de plusieurs syndicats différents
mandatés par le Congrès confédéral. Les modifications attendues intégreront les
nouvelles dispositions et orientations adoptées.

Le recueil des résolutions confédérales en vigueur est un ouvrage synthétique
des différentes motions adoptées. L'effort de synthèse portera sur l'intégration
des décisions les plus récentes, invalidant, supprimant, voire complétant par voie
de conséquence les décisions antérieures.

Le recueil des résolutions confédérales en vigueur est structuré selon deux axes
principaux :

- Fonctionnement
- Orientations

Chacun de ces axes comprend plusieurs catégories thématiques afin de favoriser
tant que possible la lisibilité de l'ouvrage.

Chaque résolution sera annotée d'un historique des différents Congrès
confédéraux ayant apporté des modifications.

Afin de conserver l'historique des différentes décisions de Congrès Confédéraux,
l'intégralité des motions adoptées est portée en annexe selon la même
classification, annotées des Congrès Confédéraux concernés.

Une fois l'ouvrage mis à jour dans des délais raisonnables, il est porté à
connaissance des syndicats par le Bureau Confédéral.

Le CCN le plus proche valide l'ouvrage.
