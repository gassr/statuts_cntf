
======================
Procedure référendaire
======================

Congrès extraordinaire de Décembre 2001 à Lille.

Le BC est chargé de la réalisation de la procédure référendaire dans tous
les cas où il en est fait mention dans les articles des statuts.

Les modalités de vote sont les mêmes que pour un Congrès.

Ne peuvent participer que les Syndicats à jour de cotisations.

Cependant, pour qu’une décision soit ratifiée, un minimum de deux tiers de
ces Syndicats doit s’être exprimé.
