
===================
Frais de délégation
===================

Congrès extraordinaire de Décembre 2001 à Lille.

Les frais de délégation sont assurés par des modalités définies par le CCN
précédent le Congrès - ou par le B.C. dans le cas d’un congrès
extraordinaire –, dans le sens d’une répartition solidaire de ces frais.
