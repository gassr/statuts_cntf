
.. index::
   pair: Règles organiques ; CNT
   pair: Pacte confédéral ; CNT
   ! Règles organiques (CNT)


.. _regles_organiques:

======================================
Règles organiques annexées aux statuts
======================================



Introduction
============

**Ces règles sont les prolongements des statuts.**

Toute décision de fonctionnement et de procédure visant à compléter ou
expliciter la mise en œuvre des statuts sans les modifier sera ajoutée
à cette annexe.

Cette annexe spéciale rassemble les motions qui concernent le fonctionnement
des instances de la CNT et les obligations complémentaires aux statuts qui
s’imposent aux syndicats.

Il s’agit de règles annexées aux statuts : aucune partie du règlement ne saurait
donc être en contradiction avec les statuts ou concerner un point qui n’est
pas d’origine statutaire.

Les présentes règles adoptées par les syndicats réunis en congrès font partie
du pacte confédéral, elles sont admises et respectées comme telles.


Règles organiques
=====================================

.. toctree::
   :maxdepth: 4

   courantes/index
   historique/index
