

.. _article_18_statuts_CNT_2012:

===================================================
Article 18 Prix de la carte, caisse de solidarité
===================================================

Le prix de la carte est fixé par décision du Congrès.

Son montant est reversé à la **Caisse de solidarité**.

La part de la Confédération sur le timbre de la cotisation mensuelle est
déterminée par le Congrès.
