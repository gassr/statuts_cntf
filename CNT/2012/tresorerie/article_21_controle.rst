
.. _article_21_statuts_CNT_2012:

==================================================
Article 21 Commission de contrôle de la trésorerie
==================================================

II est constitué à l’occasion de chaque congrès et CCN une Commission de
contrôle.

Elle est chargée de la vérification de la comptabilité, du contrôle des
opérations financières de la C.N.T., ainsi que de la vérification des
conditions de cotisations exigées - des régions au CCN et des syndicats
au congrès - pour leur participation.

Elle devra établir à l’occasion de chaque C.C.N. et de chaque Congrès un
rapport sur la situation financière qui sera présenté à chaque organisation
participante.
