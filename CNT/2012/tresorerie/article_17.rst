
.. index::
   pair: Trésorerie ; CNT
   pair: Trésorerie CNT; Caisse de solidarité
   pair: Trésorerie CNT; Caisse internationale
   pair: Carte; Confédérale



.. _article_17_statuts_CNT_2012:

======================================
Article 17 Les cotisations syndicales
======================================

Les Syndicats encaissent les cotisations syndicales.

Les cotisations syndicales par adhérentE sont ventilées comme suit:

- achat de la carte confédérale annuelle et des timbres au Bureau confédéral,
  livrés au Syndicat par le canal de l’Union locale et/ou de l’Union régionale ;
- versement du forfait mensuel à la Fédération d’industrie ;
- versement du forfait mensuel à l’Union locale et à l’Union régionale ;
- versement à la Caisse confédérale de solidarité ;
- le solde restant au Syndicat.

**La carte confédérale et les timbres sont obligatoires** et doivent être délivrés
par tous les Syndicats à leurs adhérentEs.
