

.. _article_25_statuts_CNT_2012:

========================
TITRE VI Article 25 2012
========================

Tout cas litigieux non prévu sera soumis à la plus prochaine réunion du C.C.N.,
et tranché selon l'esprit des présents statuts.
