
.. index::
   pair: Article ; 24
   ! 24

.. _article_24_statuts_CNT_2012:

=====================================================================
TITRE VI Article 24 non respect des Statuts et règles organiques 2012
=====================================================================

.. seealso::

   - :ref:`motion_43_2012`

Le non respect des Statuts et règles organiques issues du Congrès est un motif
**d’exclusion**.

Dans la mesure du possible, tout conflit existant entre adhérentEs d’un Syndicat
se règle à l’intérieur de celui-ci.

Si toutefois un ou plusieurs adhérentEs s’estiment bafouéEs dans leurs droits
par la décision prise à l’issue du conflit, ceux-ci ont encore un recours
devant leur Union régionale et/ou leur Fédération d’industrie.

Tout conflit existant entre structures de la CNT, quelles qu’elles soient, doit
être évoqué et résolu par les Congrès des Syndicats de l’aire géographique et/ou
d’industrie concernées dans la mesure où ce conflit les concerne respectivement.
Toute présentation de conflit devra être inscrite à l’ordre du jour de l’instance
concernée.

Dans le cas d’incapacité à résoudre le conflit au sein des structures
géographiques et/ou d’industrie, et dans le cas ou le conflit dépasserait le
simple cadre interne d’une union géographique ou d’industrie, ou encore dans
le cas où il impliquerait directement des organismes confédéraux, celui-ci
devra alors être soumis au prochain C.C.N. (ou au C.C.N. extraordinaire
convoqué dans les conditions prévues à l'article 4), qui a pouvoir de décision provisoire, pouvant
aller jusqu'à la suspension de la structure incriminée.

**La structure incriminée peut faire appel devant le Congrès**.

Le Congrès seul peut se prononcer définitivement. Et il est le seul à pouvoir le
faire en cas de demande d’exclusion d’une structure de la CNT.

En cas de circonstances graves, le C.C.N.peut décider la convocation d'un
Congrès extraordinaire.

L'organisme incriminé garde le droit de présenter directement sa défense soit au
C.C.N., soit au Congrès. Tout conflit présenté au C.C.N. ou au Congrès devra
être inscrit à l'ordre du jour dans les délais.
