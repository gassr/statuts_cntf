
.. index::
   pair: Article; 29

.. _article_29_statuts_CNT_2012:

=================================================
TITRE VI Article 29 Autonomie des structures 2012
=================================================

.. seealso::

   - :ref:`contre_motion_11_etpic30`


L’autonomie de chaque structure consiste en la liberté de pouvoir s’abstenir
quant aux décisions qui ne lui conviennent pas, sans aller publiquement à
l’encontre de ces décisions et dans la limite du respect des présents Statuts
et des règles organiques.
