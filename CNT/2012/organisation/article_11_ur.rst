
.. index::
   pair: CNT organisation; Unions Régionales
   pair: CNT organisation; Unions Locales
   pair: Unions Régionales ; Statuts
   pair: Unions Locales ; Statuts

.. _article_11_statuts_CNT_2012:

===========================================
Article 11 Unions régionales et locales
===========================================

L'ensemble du pays est divisé en régions, dont la délimitation et le nombre
sont fixés par le Congrès confédéral.

Ces Unions régionales ont pour mission de coordonner sur un plan territorial
l’action des Syndicats et d’aider à la constitution de nouveaux Syndicats.

Les Unions régionales doivent satisfaire aux demandes et aux désirs des
travailleurs/ses en embrassant toute l'activité économique et sociale que
nécessite la défense de leurs intérêts matériels et moraux, et qu'impose leur
libération totale et définitive.

Chaque Union régionale se dote d’un bureau.

Les Unions régionales peuvent correspondre entre elles et avec les Fédérations.

A chaque C.C.N., le Bureau Confédéral donnera toute indication utile pour
permettre ces relations.

Les Unions régionales doivent établir régulièrement des rapports sur leur
activité.

**Ces rapports doivent être communiqués à leurs Syndicats adhérents, à la
C.A., au Bureau, aux autres Unions régionales et aux Fédérations.**

**Les Syndicats, dans les régions, se regroupent en Unions locales.**

Ces organisations ne sont pas décisionnelles au niveau de la Confédération.
