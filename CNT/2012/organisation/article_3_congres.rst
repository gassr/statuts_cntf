
.. index::
   pair: Statuts CNT 2012; Article 3


.. _article_3_statuts_CNT_2012:

==============================
Article 3 Décision de Congrès
==============================



Présentation
=============

La C.N.T. est administrée suivant les directives données et les
décisions  prises par les Syndicats réunis en Congrès,
**tous les deux ans**.

**A la CNT, le pouvoir appartient aux Syndicats, cellule de base de la
Confédération, et à leurs adhérents au sein des syndicats.**

La CNT fonctionne sur un mode autogestionnaire.

Cela implique une attention toute particulière à la rotation des mandats,
et au contrôle des mandaté-e-s,  responsables et révocables, par le syndicat.

La CNT refuse d'avoir recours à des permanents techniques et/ou syndicaux.

Motions concernant l'article 3 (motion 15 2012)
================================================

.. seealso:: :ref:`motion_15_2012`
