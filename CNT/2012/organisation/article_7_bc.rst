
.. _bc_statuts_CNT_2012:
.. _article_7_statuts_CNT_2012:

==========================================
Article 7 Rôle du Bureau Confederal `B.C`
==========================================

Le Bureau confédéral est l'agent d’exécution et de coordination de la C.N.T.

Il est nommé pour deux ans.

Il est élu par le Congrès.

Il est révocable par le Congrès et, en cas de circonstances graves, il peut
être suspendu par un C.C.N. qui nommera un Bureau provisoire jusqu'au
Congrès extraordinaire convoqué de droit.

Les membres du Bureau confédéral ne pourront occuper aucun poste responsable
philosophique ou religieuse.

Leur acte de candidature impliquera d'office leur démission des fonctions
qu'ils occupent.

Le :term:`B.C` doit veiller, en toute circonstance, au respect des statuts
et des  décisions de congrès et de CCN.

Les membres responsables de la C.N.T. ne peuvent se prévaloir de ce titre en
dehors de ce qui la concerne.

Dans les cas où les mandatés au :term:`B.C` et à la C.A. se trouvent confrontés
à une décision devant être prise ne relevant pas des tâches purement techniques
et ne se situant pas dans la ligne d’une action confédérale décidée en congrès,
ils doivent se consulter, et le cas échéant, le :term:`B.C` consultera aussi
les Unions régionales.

Le :term:`B.C` rendra alors une décision argumentée sur la base de ces
consultations.
