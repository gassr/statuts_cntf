
.. index::
   Candidats; Bureau Confédéral

.. _article_8_statuts_CNT_2012:

===============================================
Article 8 Candidats au Bureau Confederal `B.C`
===============================================

.. seealso::

   - :ref:`regles_organiques_elections_BC_CNT`

**Les candidatEs au Bureau Confédéral sont présentéEs par les Syndicats**.

Les Syndicats doivent faire parvenir à la C.N.T. la liste de leurs candidatEs,
pris dans leur sein ou en dehors d'eux, **au moins deux mois avant la date du
Congrès Confédéral**.

La liste des candidatEs est immédiatement communiquée à tous les Syndicats
par le Bureau confédéral.
