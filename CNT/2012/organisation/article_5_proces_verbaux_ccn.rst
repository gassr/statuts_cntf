
.. _article_5_statuts_CNT_2012:

===================================
Article 5 : procès verbaux de C.C.N
===================================

Les procès-verbaux de chacune des séances du C.C.N. donneront le nom des
régions représentées, excusées et absentes.

**Les déléguéEs sont tenuEs de rendre compte des discussions de ces divers
comités à leurs mandants**.
