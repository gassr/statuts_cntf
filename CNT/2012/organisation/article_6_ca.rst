
.. index::
   ! CA
   ! Commission Administrative
   pair: Commission ; Administrative

.. _article_6_statuts_2012:
.. _article_6_statuts_CNT_2012:

=================================================
Article 6 Commission Administrative (C.A.) 2012
=================================================

.. seealso::

   - :ref:`article_6_statuts_2014`

Dans l'intervalle des Comités confédéraux nationaux, la C.N.T. est
administrée par le :term:`Bureau Confédéral` (B.C) et
la :term:`Commission Administrative` (C.A.)  élus par le Congrès.

**La C.A. est composée des différents secrétariats confédéraux mis
en place par le congrès**.

Les membres de la CA ne pourront occuper aucun poste responsable
relevant d'un parti politique, d'une secte philosophique ou religieuse.

La nouvelle C.A. entre en fonction à l'issue du Congrès.
Les membres de la C.A. sortante sont immédiatement rééligibles.

La C.A. doit coordonner son action et agir au travers des différents
secrétariats qui la composent conformément aux accords de Congrès et de CCN.
