
.. index::
   pair: Article; 16


.. _article_16_statuts_CNT_2012:

===========================================================
TITRE IV – Article 16 Voix des syndicats au Congrès (2012)
===========================================================

.. seealso::

   - :ref:`fonctionnement_congres_confederal`
   - :ref:`article_16_statuts_CNT_2014`



Chaque Syndicat représenté au Congrès dispose d'une voix.

Chaque déléguéE ne peut représenter exceptionnellement
que **trois Syndicats  au maximum**.

Un membre du Bureau ou de la :term:`C.A.` ne peut représenter que son Syndicat.
Il ne peut détenir un mandat d'un autre Syndicat.

**Les membres de la C.A. assistent à titre consultatif, au Congrès**,
ainsi qu'un représentant de chaque Fédération d'industrie.
