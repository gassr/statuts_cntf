

.. _article_15_statuts_2012:
.. _article_15_statuts_CNT_2012:

========================================================================
TITRE IV – Article 15 Présidence de séance et comptes-rendus de Congrès
========================================================================

.. seealso::

   - :ref:`fonctionnement_congres_confederal`

Le Congrès nomme la présidence de séance.

Le compte-rendu du Congrès sera publié sous la responsabilité d’un secrétaire
au compte rendu, nommé par les congressistes dès le début des travaux de
celui-ci.

Sa publication étant assurée par le Bureau confédéral nommé par le Congrès.

Chaque Syndicat, Union locale, Union régionale, Fédération en reçoit un
exemplaire à titre gratuit.

Un duplicata de la minute sténographique, les rapports des commissions, ainsi
que les propositions déposées auprès de la présidence de séance du Congrès,
seront versées aux archives de la CNT.
