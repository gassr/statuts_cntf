


.. _article_13_statuts_CNT_2012:

=========================================
TITRE IV Article 13 Périodité des Congrès
=========================================

.. seealso::

   - :ref:`fonctionnement_congres_confederal`


Les Syndicats se réunissent en Congrès national tous les deux ans.

Le Bureau Confédéral doit veiller à ce qu’il ne s’écoule jamais plus de 30 mois
entre deux congrès.

A la demande d'un quart des Unions régionales ou de 25% des Syndicats
adhérents à la C.N.T., le B.C. sera obligé, dans le délai d'un mois, de faire un
référendum dans les Syndicats, en les informant de cette demande de Congrès
extraordinaire.

Si la convocation du Congrès extraordinaire est ratifiée, le Congrès sera
réuni dans les trois mois suivants.

Les procédures sont ensuite les mêmes que celles du Congrès ordinaire.

Ne peuvent participer au Congrès que les syndicats à jour de leurs cotisations
à la fin du quatrième mois précédant le mois du Congrès.
