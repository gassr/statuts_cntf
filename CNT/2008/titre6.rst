

================================================================
TITRE VI Règles relatives aux dispositions des articles 24 à 29
================================================================


DIVERS
======

Féminisation du vocabulaire des statuts confédéraux

26ème Congrès confédéral du 5 et 6 Décembre 1998 à Paris

Afin que soit mieux pris en compte les femmes dans nos statuts, nous souhaitons
une féminisation du vocabulaire (ex :travailleurs-se(s), etc.).
