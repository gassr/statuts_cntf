
.. index::
   pair: CNT ; Composition (2008)

.. _composition_statuts_CNT_2008:

====================
TITRE II composition
====================

.. _article_2_statuts_CNT_2008:

Article 2
==========

La C.N.T. est constituée par:

Les Syndicats d’industrie - ou intercorporatifs lorsqu’il n’est pas possible de
créer les premiers - groupés dans:

1. les Unions Locales (U.L)
2. Les Unions Régionales (U.R) de Syndicats
3. Les Fédérations d'industrie

Cette association est conçue et organisée sur des bases fédéralistes.

Les Syndicats, les Fédérations, les Unions locales et les Unions régionales
doivent déposer des Statuts en cohérence avec les Statuts de la CNT.

Nul syndicat ne peut faire partie de la C.N.T. s'il n'adhère pas à sa Fédération
d'industrie, à son Union locale, et à son Union régionale.

Les organisations adhérentes à la C.N.T. ont droit à la marque distinctive
appelée label confédéral.

L'adhésion à la CNT des Syndicats nécessite l'acquittement des cotisations et
le respect des présents Statuts.
